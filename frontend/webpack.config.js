const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


// const webpack = require('webpack');
const path = require('path');

// Input -> Output
const APP_DIR = path.resolve(__dirname, './src');
const BUILD_DIR = path.resolve(__dirname, '../archive/static/archive');
const APP3D_DIR = path.resolve( __dirname, '../app3d/static/app3d/js');

const config = {
  entry: {
    //main: `${APP_DIR}/js/app.js`,
    index: ["babel-polyfill", `${APP_DIR}/components/index.jsx`],
    //scene: `${APP_DIR}/js/scene.jsx`,
  },
  // Source map back to original code for debugging
  // See: http://blog.teamtreehouse.com/introduction-source-maps
  devtool: 'source-map',
  // Loaders let you load and bundle different types of files
  module: {
    rules: [
      {
        test: /\.jsx?/,
        include: `${APP_DIR}`,
        use: [{
          loader: 'babel-loader',
          query: {
            presets: ['react', 'es2015'],
            compact: false,
          },
        }],
      }, {
        test: /\.scss$/,
        include: `${APP_DIR}`,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
            }, {
              loader: 'sass-loader',
            }],
        }),
      }, {
        test: /\.js$/,
        exclude: /node_modules/,
        include: [`${APP_DIR}`, `${APP3D_DIR}`],
        use: [{
          loader: 'babel-loader',
          query: {
              presets: ['es2015'],
              compact: false,
            },
          }, {
            loader: 'eslint-loader',
          }],
      }, {
        test: /\.json$/,
        use: 'json-loader',
      }, {
        test: /\.(eot|svg|otf|ttf|woff|woff2)$/,
        use: 'file-loader',
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', ],
  },
  output: {
    path: BUILD_DIR,
    //filename: '/js/bundle.js',
    filename: 'js/[name].bundle.js',
  },
  // Extracts CSS from bundle into its own file
  // This may go away eventually, since it means an additional call
  plugins: [
    new ExtractTextPlugin({
      filename: `/css/[name].css`,
      allChunks: true
    }),
    new UglifyJsPlugin({
      compress: true,
      sourceMap: true
    }),
    new webpack.DefinePlugin({ // <-- key to reducing React's size
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.AggressiveMergingPlugin(), //Merge chunks
    new BundleAnalyzerPlugin(),
    new webpack.ProvidePlugin({
      Promise: 'imports-loader?this=>global!exports-loader?global.Promise!es6-promise',
      fetch: 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
    })
  ],
};

module.exports = config;
