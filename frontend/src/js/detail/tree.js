import { wrap } from './text-wrapper';

export default function Tree(App, treeOps) {

  const svg_ID = treeOps.svg_ID;
  const tag_ID = treeOps.tag_ID;
  const data = treeOps.data;

  function render() {
    d3.select(svg_ID + " svg").remove();

    const svg = d3.select("#svg")
      .append("svg")
      .attr("width", 800)
      .attr("height", 800);

    const g =
      svg
        .append("g")
        .attr("transform", "translate(0,40)");

    const hierarchy = d3.hierarchy(data);
    const tree =
      d3.tree(data)
        .size([700, 700]);

    const node = g.selectAll(".node")
       .data(tree(hierarchy).descendants())
       .enter().append("g")
         .attr("class", (d) => "node" + (d.children ? " node--internal" : " node--leaf"))
         .attr("transform", (d) => "translate(" + d.x + "," + d.y + ")")

     node.append("circle")
         .attr("r", 5)
         .style("fill", (d) => {
           if (+d.data.id === +tag_ID) {
             return App.colors.YELLOW;
           }
           return "#ffffff";
         });

    node.append("text")
      .style("fill", (d) => {
        if (+d.data.id === +tag_ID) {
          return App.colors.YELLOW;
        }
        return "#ffffff";
      })
      .attr("transform", (d) => `translate(0, 20)`)
      .style("font-size", "0.8em")
      .style("text-anchor", "MIDDLE")
      .text((d) => d.data.name)
      .call(wrap, 100);
  }

  return {
    render,
  }
};
