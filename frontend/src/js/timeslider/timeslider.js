import esLocale from '../data/es-MX.json';

class TimesliderLogic {

    constructor(domain, app, ui, select, filter, forward, backward) {
        this.domain= domain;
        this.app = app;
        this.ui = ui;
        this.select = select;
        this.filter = filter;
        this.forward = forward;
        this.backward = backward;

        d3.timeFormatDefaultLocale(esLocale);

        this.timeslider = this.create(this.ui.dom.timeslider);
    }

    /* Create */
    create(id) {
        return {
            slider: this.createSlider(id),
            control: this.createControl(id),
        };
    }

    // Create slider
    createSlider(id) {
        // Create slider size
        const size = {};

        size.height = 140;

        size.width = d3.select(`#${id}`).node().getBoundingClientRect().width - 100;

        // Create slider dom
        const dom = {};

        dom.slider = d3.select(`#${id}`).append('svg')
            .attr('id', 'slider')
            .attr('width', size.width)
            .attr('height', size.height);

        dom.x0Axis = dom.slider.append('g')
            .attr('id', 'x0Axis')
            .attr('transform', 'translate(0, 25)');

        dom.x1Axis = dom.slider.append('g')
            .attr('id', 'x1Axis')
            .attr('transform', 'translate(0, 105)');

        dom.yAxis = dom.slider.append('g')
            .attr('id', 'yAxis')
            .attr('transform', 'translate(0, 0)');

        dom.startLabel = dom.slider.append('text')
            .attr('id', 'startLabel')
            .attr('x', 0)
            .attr('y', 15)

        dom.endLabel = dom.slider.append('text')
            .attr('id', 'endLabel')
            .attr('x', size.width - 110)
            .attr('y', 15)

        // Create slider scale
        const scale = {};

        scale.x = d3.scaleTime();

        scale.y = d3.scaleOrdinal()
            .domain(['V', 'P', 'M', 'NS', 'O'])
            .range([30, 46, 62, 78, 94]);

        // Create slider axis
        const axis = {};

        axis.x = [
            d3.axisBottom(scale.x)
                .ticks(10)
                .tickPadding(5)
                .tickSize(80)
                .tickFormat(d3.timeFormat('%d %b')),

            d3.axisBottom(scale.x)
                .ticks(10)
                .tickPadding(20)
                .tickSize(0)
                .tickFormat(d3.timeFormat('%H:%M')),
        ];

        axis.y = d3.axisRight(scale.y)
            .tickSize(size.width)
            .tickFormat('');

        // Create slider resize listener
        window.addEventListener('resize', () => {
            this.update();
        });

        return {
            size: size,
            dom: dom,
            scale: scale,
            axis: axis,
            dataset: this.createDataset(dom.slider)
        };
    }

    // Create dataset
    createDataset(element) {
        const dom = {};

        dom.dataset = element.append('g')
            .attr('id', 'dataset');

        dom.attacks = dom.dataset.append('g')
            .attr('id', 'attacks');

        dom.events = dom.dataset.append('g')
            .attr('id', 'events');

        dom.coevents = dom.dataset.append('g')
            .attr('id', 'coevents');

        dom.marker = dom.dataset.selectAll('.marker')
            .data([0])
            .enter()
            .append('circle')
            .attr('class', 'marker');

        return {
            dom: dom
        };
    }

    // Create control
    createControl(id) {
        // Create control size
        const size = {};

        size.height = 140;

        size.width = 100;

        // Create control dom
        const dom = {};

        dom.control = d3.select(`#${id}`)
            .append('svg')
            .attr('id', 'control')
            .attr('width', size.width)
            .attr('height', size.height);

        dom.forward = dom.control.append('path')
            .attr('id', 'forward')
            .attr('d', d3.symbol().type(d3.symbolTriangle).size(100))
            .attr('transform', 'translate(70, 80)rotate(90)')
            .on('click', this.forward);

        dom.backward = dom.control.append('path')
            .attr('id', 'backward')
            .attr('d', d3.symbol().type(d3.symbolTriangle).size(100))
            .attr('transform', 'translate(30, 80)rotate(-90)')
            .on('click', this.backward);

        dom.keyframeLabel = dom.control.append('text')
            .attr('id', 'keyframeLabel')
            .attr('x', 35)
            .attr('y', 50);

        return {
            size: size,
            dom: dom,
        };
    }

    /* Update */
    update(domain, app, ui) {
        // Updata state
        this.domain = domain || this.domain;
        this.app = app || this.app;
        this.ui = ui || this.ui;

        // Update slider
        this.updateSlider();

        // Update control
        this.updateControl();
    }

    // Update slider
    updateSlider() {
        // Update slider size
        if (d3.select(`#${this.ui.dom.timeslider}`).node() !== null) {
            this.timeslider.slider.size.width = d3.select(`#${this.ui.dom.timeslider}`).node().getBoundingClientRect().width - this.timeslider.control.size.width;
        }

        // Updata slider dom
        this.timeslider.slider.dom.slider.attr('width', this.timeslider.slider.size.width);

        // Update slider scale
        this.timeslider.slider.scale.x.range([0, this.timeslider.slider.size.width]);
        this.timeslider.slider.scale.x.domain(this.app.filters.range);

        // Update slider axis
        this.timeslider.slider.dom.x0Axis
            .transition()
            .duration(500)
            .call(this.timeslider.slider.axis.x[0]);

        this.timeslider.slider.dom.x1Axis
            .transition()
            .duration(500)
            .call(this.timeslider.slider.axis.x[1]);

        this.timeslider.slider.axis.y
            .tickSize(this.timeslider.slider.size.width);

        this.timeslider.slider.dom.yAxis
            .call(this.timeslider.slider.axis.y);

        // Update slider labels
        this.timeslider.slider.dom.startLabel
            .text(d3.timeFormat('%d %b %Y, %H:%M')(this.app.filters.range[0]));

        this.timeslider.slider.dom.endLabel
            .attr('x', this.timeslider.slider.size.width - 110)
            .text(d3.timeFormat('%d %b %Y, %H:%M')(this.app.filters.range[1]));

        // Update slider dataset
        this.updateDataset();
    }

    // Update dataset
    updateDataset() {
        // Update events
        const events = this.timeslider.slider.dataset.dom.events
            .selectAll('.event')
            .data(this.domain.locations.reduce((acc, value) => {
                return acc.concat(value.events);
            }, []));

        events.enter()
            .append('circle')
            .attr('class', 'event')
            .merge(events)
            .attr('r', 5)
            .attr('cy', (event) => this.getY(event, 'event'))
            .style('fill', (event) => this.getColor(event))
            .on('click', (event) => { this.select([event.id]) })
            .on('mouseover', this.handleMouseOver)
            .on('mouseout', this.handleMouseOut)
            .transition()
            .duration(500)
            .attr('cx', (event) => this.getX(event))
            .attr('r', (event) => this.getRadius(event));

        events.exit()
            .attr('r', 0)
            .remove();

        // Update coevents
        const coevents = this.timeslider.slider.dataset.dom.coevents
            .selectAll('.coevent')
            .data(this.domain.coevents.filter(item => item));

        coevents.enter()
            .append('circle')
            .attr('class', 'coevent')
            .merge(coevents)
            .attr('cy', (coevent) => this.getY(coevent.event, 'coevent'))
            .on('click', (coevent) => this.select([coevent.event.id]))
            .on('mouseover', this.handleMouseOver)
            .on('mouseout', this.handleMouseOut)
            .style('fill', (coevent) => this.getColor(coevent.event))
            .transition()
            .duration(500)
            .attr('cx', (coevent) => this.getX(coevent.event));

        coevents.exit()
            .remove();

        // Update attacks
        const attacks = this.timeslider.slider.dataset.dom.attacks
            .selectAll('.attack')
            .data(this.domain.attacks.filter(item => item));

        attacks.enter()
            .append('rect')
            .attr('class', 'attack')
            .merge(attacks)
            .attr('height', 78)
            .attr('width', 5)
            .attr('x', (attack) => this.getX(attack))
            .attr('y', 23)
            .style('fill', 'rgb(233, 0, 19)')
            .style('opacity', 0.5)
            .style('cursor', 'pointer')
            .on('click', (attack) => this.select([attack.event.id]));

        attacks.exit()
            .remove();
    }

    getWidth() {
        return (70 * this.timeslider.slider.size.width) / ((this.timeslider.slider.scale.x.domain()[1].getTime() - this.timeslider.slider.scale.x.domain()[0].getTime()) / 60000);
    }

    getX(target) {
        return (this.timeslider.slider.scale.x(
                    d3.timeParse('%Y-%m-%dT%H:%M:%S')(target.timestamp)));
    }

    getY(target) {
        return this.timeslider.slider.scale.y(
                this.getNarrative(target));
    }

    getRadius(target) {
        return ((d3.timeParse('%Y-%m-%dT%H:%M:%S')(target.timestamp) >
                    this.timeslider.slider.scale.x.domain()[0] &&
                    d3.timeParse('%Y-%m-%dT%H:%M:%S')(target.timestamp) <
                    this.timeslider.slider.scale.x.domain()[1]) ?
                5 : 0);
    }

    getColor(target) {
        switch(this.getNarrative(target)) {
            case 'V': return this.ui.style.colors.VICTIMS;
            case 'M': return this.ui.style.colors.MILITARY;
            case 'P': return this.ui.style.colors.POLICE;
            case 'NS': return this.ui.style.colors.NONSTATE;
            default : return this.ui.style.colors.OTHER;
        }
    }

    getNarrative(target) {
        switch(true) {
            case target.narrative.group === 'Víctimas' :
                return 'V';
            case target.narrative.group === 'Fuerzas de seguridad' &&
                target.narrative.subgroup === 'Ejército' :
                return 'M';
            case target.narrative.group === 'Fuerzas de seguridad' &&
                target.narrative.subgroup !== 'Ejército' :
                return 'P';
            case target.narrative.group === 'Presuntos miembros del crimen organizado' :
                return 'NS';
            default : return 'O';
        }
    }

    mark(target) {
        this.unmark();

        const x = this.timeslider.slider.scale.x(d3.timeParse('%Y-%m-%dT%H:%M:%S')(target.timestamp));
        const y = 10;

        this.timeslider.slider.dataset.dom.marker
            .attr('cx', x)
            .attr('cy', y)
            .attr('r', 10)
            .style('opacity', 1);
    }

    unmark() {
        this.timeslider.slider.dataset.dom.marker
            .style('opacity', 0);
    }

    handleMouseOver() {
        d3.select(this)
            .attr('r', 7)
            .style('opacity', 1);
    }

    handleMouseOut() {
        d3.select(this)
            .attr('r', 5)
            .style('opacity', 0.45);
    }

    updateControl() {
        // Update control labels
        this.timeslider.control.dom.keyframeLabel
            .text(`${this.app.filters.keyframe + 1} / ${this.app.keyframes[this.app.district.name].length}`);
    }

}

export default TimesliderLogic;
