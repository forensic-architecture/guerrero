import '../scss/main.scss';
import sorting from '../js/data/sorting.json';

import React from 'react';

import TagGroup from './TagGroup.jsx';

class TagListPanel extends React.Component {

  constructor(props) {
    super(props);
  }

  renderTagGroupList(tagType) {
    const thisTagType = this.props.tagTypes.byId[tagType];

    if (thisTagType) {
      const groups = sorting.types[thisTagType.id];

      return groups.map((group, index) => {
        return (
          <TagGroup
            tags={this.props.tags}
            tagTypes={this.props.tagTypes}
            tagGroups={this.props.tagGroups}
            tagSubgroups={this.props.tagSubgroups}
            narratives={this.props.narratives}
            tagFilters={this.props.tagFilters}
            narrativeFilters={this.props.narrativeFilters}
            filter={this.props.filter}
            groupLabel={group}
            groupName={group}
            language={this.props.language}
            isNarrative={this.props.isNarrative}
          />
        )
      });
    }
    return '';
  }

  render() {
    return (
      <div className="react-innertabpanel">
        <ul>{this.renderTagGroupList(this.props.tagtype)}</ul>
      </div>
    );
  }
}

export default TagListPanel;
