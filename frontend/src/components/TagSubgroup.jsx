import '../scss/main.scss';
import translation from '../js/data/translation.json';
import React from 'react';

import TagFilter from './TagFilter.jsx';
import Checkbox from './Checkbox.jsx';

class TagSubgroup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isFolded: true,
        }
    }

    onClickCollapse() {
        this.setState({
            isFolded: !this.state.isFolded,
        });
    }

    isActive() {
        let filters = (this.props.isNarrative) ? this.props.narrativeFilters : this.props.tagFilters;
        return this.props.subgroup.tags.some((tag) => {
            return filters.includes(tag.id);
        });
    }

    onClickCheckbox() {
        const tagsInSubgroup = this.props.subgroup.tags.map(tag => tag.id);
        const tagFilters = this.props.tagFilters;
        const narratives = this.props.narrativeFilters;

        if (this.props.isNarrative) {
          if (this.isActive()) {
              const keepThese = narratives.filter((element) => !(tagsInSubgroup.includes(element)));
              this.props.filter({narratives: keepThese });
          } else {
              this.props.filter({narratives: narratives.concat(tagsInSubgroup.filter((element) => !(narratives.includes(element))))});
          }
        } else {
          if (this.isActive()) {
              const keepThese = tagFilters.filter((element) => !(tagsInSubgroup.includes(element)));
              this.props.filter({tags: keepThese});
          } else {
              this.props.filter({tags: tagFilters.concat(tagsInSubgroup.filter((element) => !(tagFilters.includes(element))))});
          }
        }
    }

    renderTags() {
        return this.props.subgroup.tags.map((tag) => {
            return (
                <TagFilter
                    tags={this.props.tags}
                    narratives={this.props.narratives}
                    tagFilters={this.props.tagFilters}
                    narrativeFilters={this.props.narrativeFilters}
                    filter={this.props.filter}
                    tag={tag}
                    isNarrative={this.props.isNarrative}
                />
           );
        });
    }

    getSubgroupLabel() {
        let label = this.props.subgroup.id;
        if (this.props.language === 'en-US') {
            label = translation[label] || label;
        }
        return label;
    }

    render() {
        const tags = this.props.subgroup.tags;
        const foldedClass = (!this.state.isFolded) ? '' : 'folded';

        return (
                <div
                    key={this.props.subgroup.id}
                    className={`tagsubgroup-wrapper ${foldedClass}`}
                >
                    <div className="item tagsubgroup-header">
                        <div className={`arrow ${foldedClass}`} onClick={() => this.onClickCollapse() } />
                        <div className="collapsible-item">
                            <Checkbox
                                isActive={this.isActive()}
                                label={this.getSubgroupLabel()}
                                onClickLabel={() => this.onClickCollapse()}
                                onClickCheckbox={() => this.onClickCheckbox()}
                            />
                        </div>
                    </div>
                    <ul className="tagsubgroup-content">
                        {this.renderTags()}
                    </ul>
                </div>
               );
    }
}

export default TagSubgroup;
