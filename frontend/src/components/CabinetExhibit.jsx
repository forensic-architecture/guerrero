import React from 'react';

import copy from '../js/data/copy.json';

export default class CabinetResources extends React.Component {

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  renderMainCopy() {
    return copy[this.props.language].cabinet.exhibition.map(paragraph => {
      if (paragraph.type === 'h2') return (<h2>{paragraph.text}</h2>);
      if (paragraph.type === 'highpoint') return (<div className="highpoint">{paragraph.text}</div>);
      if (paragraph.type === 'img') return (<img src={`${paragraph.src}`} />);
      if (paragraph.type === 'doc') return (
        <div className="actions">
          <div className="document-action action">
            <button><a target="_blank" href={`${paragraph.src}`}>{paragraph.text}</a></button>
          </div>
        </div>
      )
      return (<p>{paragraph.text}</p>)
    });
  }

  render() {
    return (
      <div className="cabinet-file-content">
        <h1>{copy[this.props.language].cabinet.terms.exhibition}</h1>
        <div className="title-separator" />
        <div className="cabinet-body-text">
          <div className="section">
            {this.renderMainCopy()}
          </div>
        </div>
      </div>
    )
  }
}
