import '../scss/main.scss';
import copy from '../js/data/copy.json';
import { isNotNullNorUndefined } from '../js/data/utilities';

import React from 'react';

class Card extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isFolded: true
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        if (this.state.isFolded) {
          this.props.highlight(this.props.event);
        } else {
          this.props.highlight();
        }
        this.setState({
            isFolded: !this.state.isFolded
        });
    }

    getNarrativeColorClass(narrative) {
        const group = narrative.group;
        const subgroup = narrative.subgroup;
        if (group === 'Víctimas') return 'victims';
        if (group === 'Fuerzas de seguridad'
          && subgroup === 'Ejército') return 'military';
        if (group === 'Fuerzas de seguridad'
          && subgroup !== 'Ejército') return 'police';
        if (group === 'Presuntos miembros del crimen organizado') return 'non-state';
        return 'other';
    }

    renderHeader() {
        return (
             <div className="card-collapsed">
                 {this.renderWarning()}
                 {this.renderNarrative()}
                 {this.renderIncidents()}
                 {this.renderCommunicationDeets()}
             </div>
        );
    }

    renderWarning() {
      const warning_lang = copy[this.props.language].cardstack.warning;

      if (this.props.event.tags.some(tag => {
          return (tag.name === 'contradicción' ||
           tag.name === 'declaración con sospecha de tortura')
        })) {
            return (<div className="warning event-card-section">{warning_lang}</div>);
        }
    }

    renderNarrative() {
        const narrative_lang = copy[this.props.language].cardstack.narrative;
        const tagSource = this.props.event.narrative;

        const colorType = this.getNarrativeColorClass(tagSource);
        return (
            <div className="event-card-section narrative">
                <h4>{narrative_lang}</h4>
                <p><span className={`color-narrative ${colorType}`} />{tagSource.name}</p>
            </div>
       );
    }

    renderCommunicationDeets() {
      const event = this.props.event;
      if (event.hasOwnProperty('receiver') || event.hasOwnProperty('transmitter')) {
          const communication_lang = copy[this.props.language].cardstack.communication;
          const transmitter_lang = copy[this.props.language].cardstack.transmitter;
          const receiver_lang = copy[this.props.language].cardstack.receiver;

          const transmitter = (event.transmitter) ? event.transmitter.name : '-';
          const receiver = (event.receiver) ? event.receiver.name : '-';
          const transmitter_loc = (event.location_transmitter) ? event.location_transmitter.name : '-';
          const receiver_loc = (event.location_receiver) ? event.location_receiver.name : '-';

        return (
          <div className="event-card-section communication">
              <h4>{communication_lang}</h4>
              <p>{`De: ${transmitter} (${transmitter_loc})`}</p>
              <p>{`A: ${receiver} (${receiver_loc})`}</p>
          </div>
        )
      }
      return '';
    }

    renderContent() {
        if (this.state.isFolded) {
            return (<div className="card-bottomhalf folded"></div>);
        } else if (this.props.isFetchingEvents) {
            return (
                <div className="card-bottomhalf">
                    {this.renderSpinner()}
                </div>
           );
        } else {
            if (!this.props.event.hasOwnProperty('receiver')
              && !this.props.event.hasOwnProperty('transmitter')) {
              return (
                  <div className="card-bottomhalf">
                      {this.renderSummary()}
                      {this.renderTimestamp()}
                      {this.renderLocation()}
                      {this.renderTags()}
                      {this.renderSource()}
                  </div>
              );
            } else {
              return (
                  <div className="card-bottomhalf">
                      {this.renderSummary()}
                      {this.renderTimestamp()}
                      {this.renderTags()}
                      {this.renderSource()}
                  </div>
              );
            }
        }
    }

    renderIncidents() {
        const incident_type_lang = copy[this.props.language].cardstack.incident_type;
        const incidentTags = this.props.event.tags.filter(tag => tag.type === 'incident_type');

        return (
            <div className="event-card-section event-type">
                <h4>{incident_type_lang}</h4>
                {incidentTags.map((tag, idx) => {
                        return (
                            <span className={
                                (tag.name === 'contradicción' ||
                                 tag.name === 'declaración con sospecha de tortura') ?
                                    ' flagged' : ''}>
                                {tag.name}{(idx < incidentTags.length - 1) ? ',' : ''}
                            </span>
                        );
                    })
                }
            </div>
       );
    }

    renderSummary() {
        const summary = copy[this.props.language].cardstack.description;
        return (
            <div className="event-card-section summary">
                <h4>{summary}</h4>
                <p>{this.props.event.name}</p>
            </div>
        );
    }

    renderTags() {
      const people_lang = copy[this.props.language].cardstack.people;
        const peopleTags = this.props.event.tags.filter(tag => tag.type === 'people');

        return (
            <div className="event-card-section tags">
                <h4>{people_lang}</h4>
                <p>{peopleTags.map((tag, idx) => {
                    return (
                        <span className="tag">
                            {tag.name}
                            {(idx < peopleTags.length - 1) ? ',' : ''}
                        </span>
                    );
                })}</p>
            </div>
        );
    }

    renderLocation() {
        const location_lang = copy[this.props.language].cardstack.location;
        if (isNotNullNorUndefined(this.props.event.location)) {
          return (
              <p className="event-card-section location">
                  <h4>{location_lang}</h4>
                  <p>{this.props.event.location.name}</p>
              </p>
          );
        } else {
          return (
              <p className="event-card-section location">
                  <h4>{location_lang}</h4>
                  <p>Sin localización conocida.</p>
              </p>
          );
        }
    }

    renderSource() {
        const source_lang = copy[this.props.language].cardstack.source;
        return (
            <div className="event-card-section source">
                <h4>{source_lang}</h4>
                <p>{this.props.event.source}</p>
            </div>
       );
    }


    renderTimestamp() {
        const daytime_lang = copy[this.props.language].cardstack.timestamp;
        const estimated_lang = copy[this.props.language].cardstack.estimated;

        if (isNotNullNorUndefined(this.props.event.timestamp)) {
            const timestamp = this.props.tools.parser(this.props.event.timestamp);
            const timelabel = this.props.tools.formatterWithYear(timestamp);
            const timecode_precision = copy[this.props.language].timecode[this.props.event.timecode_precision];
            return (
                <div className="event-card-section timestamp">
                    <h4>{daytime_lang}</h4>
                    {timelabel}
                    <span className="estimated-timestamp">({timecode_precision})</span>
                </div>
           );
        } else {
            return (
                <div className="event-card-section timestamp">
                    <h4>{daytime_lang}</h4>
                    Hora no conocida
                </div>
            );
        }
    }

    renderSpinner() {
        return (
            <div className="spinner">
                <div className="double-bounce1"></div>
                <div className="double-bounce2"></div>
            </div>
       );
    }

    renderArrow() {
      let classes = (this.state.isFolded) ? 'arrow-down folded' : 'arrow-down';
      return (
          <div className="card-toggle" onClick={() => this.toggle()}>
              <p><i className={classes}></i></p>
          </div>
      );
    }

    render() {
        if (this.props.isLoading) {
            return (
                <li className='event-card'>
                  <div className="card-bottomhalf">
                      {this.renderSpinner()}
                  </div>
                </li>
            );
        } else {
            return (
                <li
                    className='event-card'
                >
                    {this.renderHeader()}
                    {this.renderContent()}
                    {this.renderArrow()}
                </li>
           );
        }
    }
}

export default Card;
