import '../scss/main.scss';
import copy from '../js/data/copy.json';

import React from 'react';

import TimesliderLogic from '../js/timeslider/timeslider.js';

class Timeslider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isFolded: false};

        this.handleClick = this.handleClick.bind(this);
        this.handleForward = this.handleForward.bind(this);
        this.handleBackward = this.handleBackward.bind(this);
    }

    componentDidMount() {
        this.timeslider = new TimesliderLogic(this.props.domain, this.props.app, this.props.ui, this.props.select, this.props.filter, this.handleForward, this.handleBackward);
        this.timeslider.update(this.props.domain, this.props.app, this.props.ui);
    }

    componentWillUpdate(nextProps) {
        this.timeslider.update(nextProps.domain, nextProps.app, nextProps.ui);
    }

    handleForward() {
        if (this.props.app.filters.keyframe < this.props.app.keyframes[this.props.app.district.name].length - 1) {
            this.props.select();
            this.updateKeyframe(this.props.app.filters.keyframe + 1);
        }
    }

    handleBackward() {
        if (this.props.app.filters.keyframe > 0) {
            this.props.select();
            this.updateKeyframe(this.props.app.filters.keyframe - 1);
        }
    }

    updateKeyframe(keyframe) {
        this.props.filter({
            keyframe: keyframe
        });
    }

    handleClick() {
        this.setState((prevState) => {
            return {isFolded: !prevState.isFolded};
        });
    }

    render() {
        let classes = `timeslider-wrapper ${(this.state.isFolded) ? ' folded' : ''}`;
        let labels = copy[this.props.app.language].timeslider.labels.map(label => <div className='timeslider-label'>{label}</div>);

        return (
                <div className={classes}>
                <div className="timeslider-header">
                <div className="timeslider-toggle" onClick={this.handleClick}>
                <p><i className="arrow-down"></i></p>
                </div>
                </div>
                <div className="timeslider-content">
                <div className="timeslider-labels">
                {labels}
                </div>
                <div id="timeslider" className="timeslider" />
                </div>
                </div>
               );
    }
}

export default Timeslider;
