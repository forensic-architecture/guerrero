import '../scss/main.scss';
import copy from '../js/data/copy.json';

import React from 'react';

import TimelineLogic from '../js/timeline/timeline.js';

class Timeline extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isFolded: false};
    }

    componentDidMount() {
        const domain = {
            events: this.props.events,
            coevents: this.props.coevents,
            attacks: this.props.attacks
        }
        const app = {
            range: this.props.range,
            selected: this.props.selected,
            language: this.props.language
        }
        const ui = {
            colors: this.props.colors,
            tools: this.props.tools,
            dom: this.props.dom
        }

        this.timeline = new TimelineLogic(app, ui, this.props.select, this.props.filter);
        this.timeline.update(domain, app);
        this.timeline.render(domain);
    }

    componentWillReceiveProps(nextProps) {
        const domain = {
            events: nextProps.events,
            coevents: nextProps.coevents,
            attacks: nextProps.attacks
        }
        const app = {
            range: nextProps.range,
            selected: nextProps.selected,
            language: nextProps.language
        }

        this.timeline.update(domain, app);
        this.timeline.render(domain);
    }

    onClickArrow() {
        this.setState((prevState, props) => {
            return {isFolded: !prevState.isFolded};
        });
    }

    renderLabels() {
        const labels = copy[this.props.language].timeline.labels;
        return labels.map(label => <div className="timeline-label">{label}</div>);
    }

    render() {
        const labels_title_lang = copy[this.props.language].timeline.labels_title;
        const info_lang = copy[this.props.language].timeline.info;
        let classes = `timeline-wrapper ${(this.state.isFolded) ? ' folded' : ''}`;
        const date0 = this.props.tools.formatterWithYear(this.props.range[0]);
        const date1 = this.props.tools.formatterWithYear(this.props.range[1]);
        return (
            <div className={classes}>
                <div className="timeline-header">
                    <div className="timeline-toggle" onClick={() => this.onClickArrow()}>
                        <p><i className="arrow-down"></i></p>
                    </div>
                    <div className="timeline-info">
                      <p>{info_lang}</p>
                      <p>{date0} - {date1}</p>
                    </div>
                </div>
                <div className="timeline-content">
                    <div className="timeline-labels">
                        <div className="timeline-label-title">{labels_title_lang}</div>
                        {this.renderLabels(this.props.labels)}
                    </div>
                    <div id="timeline" className="timeline" />
                </div>
            </div>
        );
    }
}

export default Timeline;
