import React from 'react';

const Header = ({ onClickHeader }) => {
  return (
    <div className='header' onClick={() => { /*onClickHeader()*/ }}>
      <div className='header-title'>
        <a href="#">Caso Ayotzinapa</a>
      </div>
      <button className="side-menu-burg" >
        <span/>
      </button>
    </div>
  );
}

export default Header;
