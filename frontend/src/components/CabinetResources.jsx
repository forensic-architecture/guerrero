import React from 'react';

import copy from '../js/data/copy.json';

export default class CabinetResources extends React.Component {

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="cabinet-file-content">
        <h1>{copy[this.props.language].cabinet.terms.resources}</h1>
        <div className="title-separator" />
        <div className="cabinet-body-text">
          <h2>{copy[this.props.language].cabinet.resources.reports}</h2>
          <img src="/static/archive/img/reportPic.jpg"/>
          <div className="actions">
            <div className="document-action action">
              <button><a target="_blank" href="http://prensagieiayotzi.wixsite.com/giei-ayotzinapa/informe-">GIEI</a></button>
            </div>
            <div className="document-action action">
              <button><a target="_blank" href="http://www.megustaleer.mx/libro/una-historia-oral-de-la-infamia/MX13469">Una historia oral de la infamia - John Gibler</a></button>
            </div>
          </div>
          <h2>{copy[this.props.language].cabinet.resources.data}</h2>
          <div className="actions">
            <div className="document-action action">
              <button><a target="_blank" href="/static/archive/downloads/DataPackage.zip">GIEI I & II (CSV)</a></button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
