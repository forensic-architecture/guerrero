import '../scss/main.scss';

import React from 'react';

import Cabinet from './Cabinet.jsx';
import Dashboard from './Dashboard.jsx';

class App extends React.Component {

  render() {
    return (
      <div>
        <Dashboard />
      </div>
    );
  }
}

export default App;
