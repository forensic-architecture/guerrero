import '../scss/main.scss';

import React from 'react';

import Timeline from './Timeline.jsx';
import Timeslider from './Timeslider.jsx';

class TimeRack extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        if( this.props.ui.flags.isView2d ) {
            return (
                <Timeline
                    events={this.props.domain.events.filter(item => item)}
                    coevents={this.props.domain.coevents.filter(item => item)}
                    attacks={this.props.domain.attacks.filter(item => item)}
                    range={this.props.app.filters.range}
                    selected={this.props.app.selected}
                    language={this.props.app.language}
                    colors={this.props.ui.style.colors}
                    tools={this.props.ui.tools}
                    dom={this.props.ui.dom}
                    select={this.props.select}
                    filter={this.props.filter}
                    highlight={this.props.highlight}
                />
            );
        }
        else {
            return(
                <Timeslider
                    domain={this.props.domain}
                    app={this.props.app}
                    ui={this.props.ui}
                    select={this.props.select}
                    filter={this.props.filter}
                    highlight={this.props.highlight}
                />
            );
        }
    }
}

export default TimeRack;
