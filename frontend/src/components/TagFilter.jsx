import '../scss/main.scss';

import React from 'react';

import Checkbox from './Checkbox.jsx';

class TagFilter extends React.Component {

    constructor(props) {
        super(props);
    }

    isActive() {
      if (this.props.isNarrative) {
        return this.props.narrativeFilters.includes(this.props.tag.id);
      }
      return this.props.tagFilters.includes(this.props.tag.id);
    }

    onClickTag() {
        if (this.isActive()) {
            this.props.filter({
                tags: this.props.tagFilters.filter(element => element !== this.props.tag.id)
            });
        } else {
            this.props.filter({
                tags: this.props.tagFilters.concat(this.props.tag.id)
            });
        }
    }

    onClickNarrative() {
        if (this.isActive()) {
            this.props.filter({
                narratives: this.props.narrativeFilters.filter(element => element !== this.props.tag.id)
            });
        } else {
            this.props.filter({
                narratives: this.props.narrativeFilters.concat(this.props.tag.id)
            });
        }
    }

    renderTag() {
        const tag = this.props.tag;
        let classes = (this.isActive()) ? 'tag-filter active' : 'tag-filter';
        let label = `${tag.name} ( ${tag.mentions} )`;
        if (this.props.isShowTree) {
          label = `${tag.group} > ${tag.subgroup} > ${tag.name} ( ${tag.mentions} )`;
        }
        return (
            <li
                key={this.props.tag.id}
                className={classes}
            >
                <Checkbox
                    isActive={this.isActive()}
                    label={label}
                    onClickLabel={() => this.onClickTag()}
                    onClickCheckbox={() => this.onClickTag()}
                />
            </li>
        );
    }

    renderNarrative() {
      const narrative = this.props.narratives[this.props.tag.id];
      let classes = (this.isActive()) ? 'tag-filter active' : 'tag-filter';

      if (narrative) {
          return (
            <li
                key={this.props.tag.id}
                className={classes}
            >
              <Checkbox
                isActive={this.isActive()}
                label={`${narrative.name} ( ${narrative.counts} )`}
                onClickLabel={() => this.onClickNarrative()}
                onClickCheckbox={() => this.onClickNarrative()}
              />
            </li>
          );
      }
      return (<div/>);
    }

    render() {
        if (this.props.isNarrative) return (this.renderNarrative());
        return (this.renderTag());
    }
}

export default TagFilter;
