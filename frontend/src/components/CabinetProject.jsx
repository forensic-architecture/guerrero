import React from 'react';

import copy from '../js/data/copy.json';

export default class CabinetResources extends React.Component {

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  renderMainCopy() {
    return copy[this.props.language].cabinet.about.map(paragraph => {
      if (paragraph.type === 'highpoint') return (<div className="highpoint">{paragraph.text}</div>);
      if (paragraph.type === 'h2') return (<h2>{paragraph.text}</h2>);
      if (paragraph.type === 'i') return (<h3><i>{paragraph.text}</i></h3>);
      if (paragraph.type === 'img') return (<img src={`${paragraph.src}`} />);
      return (<p>{paragraph.text}</p>)
    });
  }

  render() {
    return (
      <div className="cabinet-file-content">
        <h1>{copy[this.props.language].cabinet.terms.untertitle}</h1>
        <div className="title-separator" />
        <div className="cabinet-body-text">
          {this.renderMainCopy()}
        </div>
      </div>
    )
  }
}
