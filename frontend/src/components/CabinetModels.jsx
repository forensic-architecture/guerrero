import React from 'react';
import copy from '../js/data/copy.json';

export default class CabinetModels extends React.Component {

    renderButton(districtId, districtClass) {
        const language = this.props.language;

        if (this.props.isWebGL) {
          return (
            <div>
              <div className={`primary-action not-on-mobile ${districtClass}`} onClick={() => this.props.goToDistrict(districtId)}>
                <button>{(this.props.isFetching) ? copy[language].loading : copy[language].cabinet.models.action}</button>
              </div>
              <div className={`primary-action only-on-mobile ${districtClass}`}>
                <button>{copy[language].cabinet.models.notOnMobile}</button>
              </div>
            </div>
          );
        } else {
          return (
            <div>
              <div className={`primary-action not-on-mobile ${districtClass}`}>
                <button>{copy[language].cabinet.models.notWebGL}</button>
              </div>
              <div className={`primary-action only-on-mobile ${districtClass}`}>
                <button>{copy[language].cabinet.models.notOnMobile}</button>
              </div>
            </div>
          );
        }
    }

    render() {
        const language = this.props.language;
        return (
          <div className="cabinet-file-content">
            <h1>{copy[language].cabinet.terms.models}</h1>
            <div className="title-separator" />
            <div className="cabinet-body-text">
              <p>{copy[language].cabinet.models.overview}</p>
              <div className="section">
                <h2>{copy[language].cabinet.models.JA.title}</h2>
                {this.renderButton(9, 'ja')}
                <p>{copy[language].cabinet.models.JA.text}</p>
              </div>
              <div className="section">
                <h2>{copy[language].cabinet.models.PJ.title}</h2>
                {this.renderButton(7, 'pj')}
                <p>{copy[language].cabinet.models.PJ.text}</p>
              </div>
              <div className="section">
                <h2>{copy[language].cabinet.models.ST.title}</h2>
                {this.renderButton(8, 'st')}
                <p>{copy[language].cabinet.models.ST.text}</p>
              </div>
            </div>
          </div>
        );
    }
}
