import React from 'react';

import copy from '../js/data/copy.json';

export default class CabinetTiles extends React.Component {

    constructor(props) {
      super(props);
      this.state = {};
    }

    renderVideoBg() {
      return (
        <div className="fullscreen-bg">
            <video muted autoPlay preload="auto" className="fullscreen-bg__video">
              <source src="/static/archive/video/landing.mp4" type="video/mp4" />
          </video>
        </div>
      );
    }

    renderVideoLanding() {
      if (this.state.showVideo) {
        return (
          <div className="fullscreen" onClick={() => { this.setState({ showVideo: false })}}>
            <button style={{ top: '20px', left: '20px', zIndex: '3' }} className='side-menu-burg is-active' onClick={() => { this.setState({ showVideo: false })}}>
              <span/>
            </button>
            <div className="video">
              <div className="wrapper">
                <iframe
                  src="https://player.vimeo.com/video/232341416"
                  width="500" height="281" frameBorder="0" webkitAllowFullScreen mozAllowFullScreen allowFullScreen
                >
                </iframe>
              </div>
            </div>
          </div>
        );
      }
      return (<div></div>);
    }

    componentDidMount() {
      setTimeout(() => {
        this.setState({ showVideo: true });
      }, 20000);
    }

    renderExplore() {
      const explore = copy[this.props.app.language].cabinet.terms.explore;
      if (!this.props.isMobile) {
        return (
          <div className="tile-row">
            <div onClick={() => this.props.closeCabinet()} className="tile full tile00">
              <div className="tile-content">
                <svg x="0px" y="0px" width="28px" height="28px" viewBox="0 0 40 40">
                    <line strokeLinejoin="round" x1="14.102" y1="29.93" x2="25.781" y2="33.123"/>
                    <polyline strokeLinejoin="round" points="23.734,11.443 25.781,33.123
                    38.659,30.479 32.002,10.423 23.734,11.443 "/>
                    <path strokeLinejoin="round" d="M23.753,6.609
                    c0-2.105-1.706-3.811-3.811-3.811S16.13,4.504,16.13,6.609c0,0.729,0.208,1.407,0.563,1.986h-0.005l3.254,5.605l3.253-5.605H23.19
                    C23.545,8.016,23.753,7.338,23.753,6.609z"/>
                    <polyline strokeLinejoin="round" points="17,9.134 15.15,8.499 6.647,12.605
                    1.842,32.774 14.102,29.93 15.15,8.499 "/>
                    <line strokeLinejoin="round" x1="21.906" y1="10.816" x2="23.734" y2="11.443"/>
                </svg>
                <div className="label">{explore} &rarr;</div>
              </div>
            </div>
          </div>
        )
      }
    }

    render() {
        const lang = copy[this.props.app.language].cabinet.terms;
        return (
          <div className="cabinet-cover">
              {this.renderVideoBg()}
              {this.renderVideoLanding()}
              <div className="content">
                  <div className="cabinet-cover-header">
                      <div className="main-title">
                        <div className="title">{lang.title}</div>
                      </div>
                      <div className="untertitle">{lang.untertitle}</div>
                  </div>
                  <div className="cabinet-cover-content">
                      {this.renderExplore()}
                      <div className="tile-row">
                          <div onClick={() => this.props.showFiles(0)} className="tile"><span>{lang.about}</span></div>
                          <div onClick={() => this.props.showFiles(1)} className="tile"><span>{lang.methodology}</span></div>
                      </div>
                      <div className="tile-row">
                          <div onClick={() => this.props.showFiles(2)} className="tile"><span>{lang.videos}</span></div>
                          <div onClick={() => this.props.showFiles(3)} className="tile"><span>{lang.models}</span></div>
                      </div>
                      <div className="tile-row">
                          <div onClick={() => this.props.showFiles(4)} className="tile"><span>{lang.resources}</span></div>
                          <div onClick={() => this.props.showFiles(5)} className="tile"><span>{lang.exhibition}</span></div>
                      </div>
                      <div className="secondary-action" onClick={() => this.props.toggleLanguage()}>
                        {(this.props.app.language === 'es-MX') ? 'In English' : 'En Español'}
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}
