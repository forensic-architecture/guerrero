import '../scss/main.scss';
import Map from '../js/map/map.js';
import { areEqual } from '../js/data/utilities.js';

import React from 'react';

class View2D extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const domain = {
            locations: this.props.locations,
            coevents: this.props.coevents,
            attacks: this.props.attacks,
            routes: this.props.routes,
            sites: this.props.sites,
        }
        const app = {
            views: this.props.views,
            selected: this.props.selected,
            highlighted: this.props.highlighted
        }
        const ui = {
            colors: this.props.colors,
            dom: this.props.dom
        }

        this.map = new Map(app, ui, this.props.select, this.props.activateDistrict);
        this.map.update(domain, app);
        this.map.render(domain);
    }

    componentWillReceiveProps(nextProps) {
        const domain = {
            locations: nextProps.locations,
            coevents: nextProps.coevents,
            attacks: nextProps.attacks,
            routes: nextProps.routes,
            sites: nextProps.sites,
        }
        const app = {
            views: nextProps.views,
            selected: nextProps.selected,
            highlighted: nextProps.highlighted
        }

        if (this.props.domain !== domain || this.props.app !== app) {
            this.map.update(domain, app);
            this.map.render(domain);
        }
    }

    render() {
        return (
            <div className='map-wrapper'>
                <div id="map" />
            </div>
       );
    }

}

export default View2D;
