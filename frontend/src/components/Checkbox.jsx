import '../scss/main.scss';

import React from 'react';

export default class Checkbox extends React.Component {

  constructor(props) {
    super(props);
  }

  render () {
    let classes = (this.props.isActive) ? 'item active' : 'item';

    return (
      <div className={classes}>
        <span onClick={() => this.props.onClickLabel()}>{this.props.label}</span>
        <button onClick={() => this.props.onClickCheckbox()}>
          <div className="checkbox" />
        </button>
      </div>
    );
  }
}
