import '../scss/main.scss';
import copy from '../js/data/copy.json';

import React from 'react';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import TagListPanel from './TagListPanel.jsx';

export default class PeoplePanel extends React.Component {

  constructor(props) {
    super(props);
    this.state = { tabIndex: 0 };
  }

  render() {
    const people_lang = copy[this.props.language].toolbar.panels.people;
    const mentions_lang = copy[this.props.language].toolbar.panels.mentions;
    const narratives_lang = copy[this.props.language].toolbar.panels.narratives;

    return (
      <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
        <TabList>
          <Tab className="people-tab">{people_lang.mention}</Tab>
          <Tab className="people-tab">{people_lang.narrative}</Tab>
        </TabList>
        <TabPanel>
          <p>{mentions_lang.overview}</p>
          <TagListPanel
            tags={this.props.tags}
            tagGroups={this.props.tagGroups}
            tagSubgroups={this.props.tagSubgroups}
            tagTypes={this.props.tagTypes}
            narratives={this.props.narratives}
            tagFilters={this.props.tagFilters}
            narrativeFilters={this.props.narrativeFilters}
            filter={this.props.filter}
            language={this.props.language}
            tagtype={'people'}
          />
        </TabPanel>
        <TabPanel>
          <div className="react-innertabpanel">
            <p>{narratives_lang.overview}</p>
            <TagListPanel
              tags={this.props.tags}
              tagGroups={this.props.tagGroups}
              tagSubgroups={this.props.tagSubgroups}
              tagTypes={this.props.tagTypes}
              narratives={this.props.narratives}
              tagFilters={this.props.tagFilters}
              narrativeFilters={this.props.narrativeFilters}
              filter={this.props.filter}
              tagtype={'people'}
              language={this.props.language}
              isNarrative={true}
            />
          </div>
        </TabPanel>
        </Tabs>
    )
  }
}
