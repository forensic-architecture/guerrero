import initial from '../store/initial.js';

import {UPDATE_DOMAIN} from '../actions';

function updateDomain(domainState, action) {
    return Object.assign({}, domainState, action.domain);
}

function domain(domainState = initial.domain, action) {
    switch(action.type) {
        case UPDATE_DOMAIN : return updateDomain(domainState, action);
        default : return domainState;
    }
}

export default domain;
