import { combineReducers } from 'redux'

import domain from './domain.js'
import app from './app.js'
import ui from './ui.js'

const rootReducer = combineReducers({
    domain,
    app,
    ui
});

export default rootReducer;
