import { createSelector } from 'reselect'

// Input selectors
export const getAttacks = state => state.domain.attacks;
export const getCoevents = state => state.domain.coevents;
export const getDistricts = state => state.domain.districts;
export const getEvents = state => state.domain.events;
export const getLocations = state => state.domain.locations;
export const getNarratives = state => state.domain.narratives;
export const getRoutes = state => state.domain.routes;
export const getSites = state => state.domain.sites;
export const getTag_groups = state => state.domain.tag_groups;
export const getTag_subgroups = state => state.domain.tag_subgroups;
export const getTag_types = state => state.domain.tag_types;
export const getTags = state => state.domain.tags;

export const getDistrictFilter = state => state.app.district.id;
export const getKeyframeFilter = state => state.app.filters.keyframe;
export const getNarrativesFilter = state => state.app.filters.narratives;
export const getTagsFilter = state => state.app.filters.tags;
export const getRangeFilter = state => {
    if (state.app.filters.keyframe === null) {
        return state.app.filters.range;
    } else {
        return [
            d3.timeParse("%Y-%m-%dT%H:%M:%S")(state.app.keyframes[state.app.district.name][state.app.filters.keyframe].range[0]),
            d3.timeParse("%Y-%m-%dT%H:%M:%S")(state.app.keyframes[state.app.district.name][state.app.filters.keyframe].range[1]),
        ];
    }
};

// Filter events
export const getFilteredEvents = createSelector(
        [getEvents, getLocations, getTags, getNarrativesFilter, getTagsFilter, getRangeFilter],
        (events, locations, tags, narrativesFilter, tagsFilter, rangeFilter) => {
            return Object.values(events.byId).reduce((acc, value) => {
                const isNarrative = narrativesFilter.includes(value.narrative_2);
                const isTag = value.tags.some((tag) => tagsFilter.includes(tag));
                const isRange = (rangeFilter[0] < d3.timeParse("%Y-%m-%dT%H:%M:%S")(value.timestamp)) &&
                    (d3.timeParse("%Y-%m-%dT%H:%M:%S")(value.timestamp) < rangeFilter[1]);

                if ((isNarrative || isTag) && isRange) {
                    const event = Object.assign({}, value);
                    event.location = locations.byId[value.location];
                    event.narrative = tags.byId[value.narrative_2];
                    event.tags = Object.values(tags.byId).reduce((acc, value) => {
                        if (event.tags.includes(value.id)) {
                            acc.push(value);
                        }

                        return acc;
                    }, []);

                    acc[event.id] = event;
                }

                return acc;
            }, []);
        });

// Filter attacks
export const getFilteredAttacks = createSelector(
        [getAttacks, getEvents, getTags, getLocations, getRangeFilter],
        (attacks, events, tags, locations, rangeFilter) => {
            return Object.values(attacks.byId).reduce((acc, value) => {
                const isRangeStart = (rangeFilter[0] < d3.timeParse("%Y-%m-%dT%H:%M:%S")(value.timestamp)) &&
                    (d3.timeParse("%Y-%m-%dT%H:%M:%S")(value.timestamp) < rangeFilter[1]);
                const isRangeEnd = (rangeFilter[0] < d3.timeParse("%Y-%m-%dT%H:%M:%S")(value.timestamp_to)) &&
                    (d3.timeParse("%Y-%m-%dT%H:%M:%S")(value.timestamp_to) < rangeFilter[1]);

                if (isRangeStart || isRangeEnd) {
                    const attack = Object.assign({}, value);
                    attack.event = Object.assign({}, events.byId[value.event]);

                    attack.event.location = locations.byId[events.byId[value.event].location];
                    attack.event.narrative = tags.byId[events.byId[value.event].narrative_2];
                    attack.event.tags = Object.values(tags.byId).reduce((acc, value) => {
                        if (attack.event.tags.includes(value.id)) acc.push(value);
                        return acc;
                    }, []);

                    acc[value.event] = attack;
                }

                return acc;
            }, []);
        });

// Filter coevents
export const getFilteredCoevents = createSelector(
        [getCoevents, getEvents, getLocations, getTags, getNarrativesFilter, getTagsFilter, getRangeFilter],
        (coevents, events, locations, tags, narrativesFilter, tagsFilter, rangeFilter) => {
            return Object.values(coevents.byId).reduce((acc, value) => {
                const isNarrative = narrativesFilter.includes(events.byId[value.event].narrative_2);
                const isTag = events.byId[value.event].tags.some((tag) => tagsFilter.includes(tag));
                const isRange = (rangeFilter[0] < d3.timeParse("%Y-%m-%dT%H:%M:%S")(events.byId[value.event].timestamp)) &&
                    (d3.timeParse("%Y-%m-%dT%H:%M:%S")(events.byId[value.event].timestamp) < rangeFilter[1]);

                if ((isNarrative || isTag) && isRange) {
                    const coevent = Object.assign({}, value);
                    coevent.event = events.byId[value.event];
                    coevent.event.location = locations.byId[events.byId[value.event].location];
                    coevent.event.narrative = tags.byId[events.byId[value.event].narrative_2];
                    coevent.location_receiver = locations.byId[value.location_receiver];
                    coevent.location_transmitter = locations.byId[value.location_transmitter];
                    coevent.receiver = tags.byId[value.receiver];
                    coevent.transmitter = tags.byId[value.transmitter];

                    acc[value.event] = coevent;
                }

                return acc;
            }, []);
        });

// Filter districts



// Filter locations
export const getFilteredLocations = createSelector(
        [getFilteredEvents, getLocations, getDistricts, getDistrictFilter],
        (events, locations, districts, districtId) => {
            return Object.values(locations.byId).reduce((acc, value) => {
                const location = Object.assign({}, value);
                var districtLocations = ( districtId !== -1 ) ? districts.byId[ districtId ].locations : [];
                if( districtId == -1 || districtLocations.includes( location.id ) ) {
                    location.events = location.events.filter(eventId => events[eventId]);
                    location.events = location.events.map(eventId => events[eventId]);
                }
                else {
                    location.events = [];
                }
                if (location.events.length > 0) acc.push(location);
                return acc;
            }, []);
        });

// Filter narratives
export const getFilteredNarratives = createSelector(
        [getNarratives, getEvents, getTags],
        (narratives, events, tags) => {
            return Object.values(narratives.byId).reduce((acc, value) => {
                const narrative = Object.assign({}, value);
                narrative.tag = tags.byId[narrative.id];
                acc[narrative.id] = narrative;
                return acc;
            }, []);
        });

// Filter routes
export const getFilteredRoutes = createSelector(
  [getRoutes], routes => Object.values(routes.byId));

// Filter sites
export const getFilteredSites = createSelector([getSites], sites => sites);

// Filter tags
export const getFilteredTags = createSelector([getTags], tags => tags);

export const getFilteredTagSubgroups = createSelector(
  [getTag_subgroups, getTags],
  (subgroups, tags) => {
    return Object.values(subgroups.byId).reduce((accS, valueS) => {
        const subgroup = Object.assign({}, valueS);
        subgroup.tags = tags.allIds.reduce((accT, tagId) => {
            if (subgroup.tags.includes(tagId)) {
                const tag = Object.assign({}, tags.byId[tagId]);
                accT.push(tag);
            }
            return accT;
        }, []);
        accS.push(subgroup);
        return accS;
    }, []);
});

export const getFilteredTagGroups = createSelector(
  [getTag_groups, getTag_subgroups, getTags],
  (groups, subgroups, tags) => {
      const groupsById = {};
      groups.allIds.forEach((valueG) => {
          const group = Object.assign({}, groups.byId[valueG]);
          group.subgroups = group.subgroups.map((subgroupId) => {
              const subgroup = Object.assign({}, subgroups.byId[subgroupId]);
              subgroup.tags = tags.allIds.reduce((accT, tagId) => {
                  if (subgroup.tags.includes(tagId)) {
                      const tag = Object.assign({}, tags.byId[tagId]);
                      accT.push(tag);
                  }
                  return accT;
              }, []);
              return subgroup;
          });

          groupsById[valueG] = group;
      }, []);

      return { byId: groupsById, allIds: groups.allIds }
  }
);

export const getFilteredTagTypes = createSelector(
  [getTag_types, getFilteredTagGroups],
  (types, groups) => {
      const typesById = {};
      types.allIds.forEach((typeId) => {
          const type = Object.assign({}, types.byId[typeId]);
          type.groups = type.groups.map(groupId => {
            return Object.assign({}, groups.byId[groupId]);
          });
          typesById[typeId] = type;
      });
      return { byId: typesById, allIds: types.allIds };
  },
);
