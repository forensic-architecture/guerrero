export default {
    "ja": [
        { "id": "ja0", range: ["2014-09-26T21:40:00", "2014-09-26T22:25:00"]},
        { "id": "ja1", range: ["2014-09-26T22:25:00", "2014-09-26T22:50:00"]},
        { "id": "ja2", range: ["2014-09-26T22:50:00", "2014-09-27T00:30:00"]},
        { "id": "ja3", range: ["2014-09-27T00:30:00", "2014-09-27T01:00:00"]},
        { "id": "ja4", range: ["2014-09-27T01:50:00",	"2014-09-27T05:45:00"]},
    ],
    "pj": [
        { "id": "pj0", range: ["2014-09-26T21:40:00", "2014-09-26T22:00:00"]},
        { "id": "pj1", range: ["2014-09-26T22:00:00", "2014-09-26T22:25:00"]},
        { "id": "pj2", range: ["2014-09-26T22:30:00", "2014-09-26T22:35:00"]},
        { "id": "pj3", range: ["2014-09-26T22:35:00", "2014-09-26T23:00:00"]},
        { "id": "pj4", range: ["2014-09-26T23:00:00", "2014-09-26T23:10:00"]},
        { "id": "pj5", range: ["2014-09-27T00:20:00", "2014-09-27T01:05:00"]},
    ],
    "st": [
        { "id": "st0", range: ["2014-09-26T23:30:00", "2014-09-26T23:31:00"]},
        { "id": "st1", range: ["2014-09-26T23:31:00", "2014-09-26T23:34:00"]},
        { "id": "st2", range: ["2014-09-26T23:34:00", "2014-09-26T23:39:00"]},
        { "id": "st3", range: ["2014-09-26T23:39:00", "2014-09-26T23:50:00"]},
        { "id": "st4", range: ["2014-09-26T23:50:00", "2014-09-27T00:00:00"]},
        { "id": "st5", range: ["2014-09-27T00:00:00", "2014-09-27T01:30:00"]},
        { "id": "st6", range: ["2014-09-27T01:30:00", "2014-09-27T03:10:00"]},
    ],
};
