import keyframes from './keyframes.js'

const canvas = document.createElement('canvas');
const gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');

const initial = {
    domain: {
        events: { byId: {}, allIds: { located: [], unlocated: [] } },
        coevents: { byId: {}, allIds: [] },
        tags: { byId: {}, allIds: [] },
        tag_types: { byId: {}, allIds: [] },
        tag_groups: { byId: {}, allIds: [] },
        tag_subgroups: { byId: {}, allIds: [] },
        locations: { byId: {}, allIds: [] },
        districts: { byId: {}, allIds: [] },
        attacks: { byId: {}, allIds: [] },
        narratives: { byId: {}, allIds: [] },
        sites: [],
        routes: { byId: {}, allIds: [] },
    },
    app: {
        highlighted: null,
        selected: [],
        filters: {
            range: [
                d3.timeParse("%Y-%m-%dT%H:%M:%S")("2014-09-25T12:00:00"),
                d3.timeParse("%Y-%m-%dT%H:%M:%S")("2014-09-28T12:00:00")
            ],
            tags: [],
            narratives: [],
            tag_groups: { byId: {}, allIds: [] },
            views: {
                events: true,
                coevents: false,
                routes: true,
                sites: true
            },
            keyframe: null,
        },
        district: { id: -1,  name: '', base_uri: 'static/app3d/data/scenes/' },
        keyframes: keyframes,
        base_uri: 'http://127.0.0.1:8000/', // Modify accordingly on production setup.
        isMobile: (/Mobi/.test(navigator.userAgent)),
        isWebGL: (gl && gl instanceof WebGLRenderingContext),
        language: 'es-MX',
    },
    ui: {
        style: {
            colors: {
                WHITE: "#efefef",
                YELLOW: "#ffd800",
                MIDGREY: "rgb(44, 44, 44)",
                DARKGREY: "#232323",
                PINK: "#F28B50",//rgb(232, 9, 90)",
                ORANGE: "#F25835",//rgb(232, 9, 90)",
                RED: "rgb(233, 0, 19)",
                BLUE: "#F2DE79",//"rgb(48, 103 , 217)",
                GREEN: "#4FF2F2",//"rgb(0, 158, 86)",
                VICTIMS: "#FF0000",
                MILITARY: "#226b22",
                NONSTATE: "#671f6f",
                POLICE: "#0000bf",
                OTHER: "#d3ce2a",
            },
            palette: d3.schemeCategory10,
        },
        dom: {
            timeline: "timeline",
            timeslider: "timeslider",
            map: "map"
        },
        flags: {
            isFetchingDomain: false,
            isFetchingEvents: false,
            isView2d: true,
            isTimeline: true,
            isToolbar: false,
            isCardstack: true,
            isCabinet: true,
            isInfopopup: true,
        },
        tools: {
            formatter: d3.timeFormat("%d %b, %H:%M"),
            formatterWithYear: d3.timeFormat("%d %b %Y, %H:%M"),
            parser: d3.timeParse("%Y-%m-%dT%H:%M:%S")
        },
        components: {
            toolbarTab: false,
            cabinetFileTab: 0
        }
    }
};

export default initial;
