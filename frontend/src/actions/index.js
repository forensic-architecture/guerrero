import 'whatwg-fetch';

// Domain

export function fetchDomain() {
    return dispatch => {
        dispatch(toggleFetchingDomain());
        return fetch('static/archive/data/data.json')
            .then(response => response.json())
            .then(json => {
                dispatch(toggleFetchingDomain());
                return json;
            });
    };
}

export const UPDATE_DOMAIN = 'UPDATE_DOMAIN';
export function updateDomain(domain) {
    return {
        type: UPDATE_DOMAIN,
        domain: domain
    };
}

// App

export function fetchEvents(events) {
    return dispatch => {
        dispatch(toggleFetchingEvents());
        const urls = events.map(event => `api/event/${(event.id) ? event.id : event}?format=json`);
        return Promise.all(urls.map(url => fetch(url)
                    .then(response => response.json())
                    )
                )
            .then(json => {
                dispatch(toggleFetchingEvents());
                return json;
            });
    };
}

export const UPDATE_HIGHLIGHTED = 'UPDATE_HIGHLIGHTED';
export function updateHighlighted(highlighted) {
    return {
        type: UPDATE_HIGHLIGHTED,
        highlighted: highlighted
    };
}

export const UPDATE_SELECTED = 'UPDATE_SELECTED';
export function updateSelected(selected) {
    return {
        type: UPDATE_SELECTED,
        selected: selected
    };
}

export const UPDATE_DISTRICT = 'UPDATE_DISTRICT';
export function updateDistrict(district) {
    return {
        type: UPDATE_DISTRICT,
        district
    };
}

export const UPDATE_FILTERS = 'UPDATE_FILTERS';
export function updateFilters(filters) {
    return {
        type: UPDATE_FILTERS,
        filters: filters
    };
}

export const UPDATE_TIMERANGE = 'UPDATE_TIMERANGE';
export function updateTimeRange(range) {
    return {
        type: UPDATE_TIMERANGE,
        range
    };
}

export const RESET_ALLFILTERS = 'RESET_ALLFILTERS';
export function resetAllFilters() {
    return {
        type: RESET_ALLFILTERS
    };
}

// UI

export const TOGGLE_FETCHING_DOMAIN = 'TOGGLE_FETCHING_DOMAIN';
export function toggleFetchingDomain() {
    return {
        type: TOGGLE_FETCHING_DOMAIN
    };
}

export const TOGGLE_FETCHING_EVENTS = 'TOGGLE_FETCHING_EVENTS';
export function toggleFetchingEvents() {
    return {
        type: TOGGLE_FETCHING_EVENTS
    };
}

export const TOGGLE_VIEW = 'TOGGLE_VIEW';
export function toggleView() {
    return {
        type: TOGGLE_VIEW
    };
}

export const TOGGLE_TIMELINE = 'TOGGLE_TIMELINE';
export function toggleTimeline() {
    return {
        type: TOGGLE_TIMELINE
    };
}

export const TOGGLE_LANGUAGE = 'TOGGLE_LANGUAGE';
export function toggleLanguage(language) {
    return {
        type: TOGGLE_LANGUAGE,
        language,
    }
}

export const OPEN_TOOLBAR = 'OPEN_TOOLBAR';
export function openToolbar(toolbarTab = 0) {
    return {
        type: OPEN_TOOLBAR,
        toolbarTab: toolbarTab,
    };
}

export const CLOSE_TOOLBAR = 'CLOSE_TOOLBAR';
export function closeToolbar() {
    return {
        type: CLOSE_TOOLBAR
    };
}

export const OPEN_CABINET = 'OPEN_CABINET';
export function openCabinet(tabNum) {
    return {
        type: OPEN_CABINET,
        tabNum: tabNum,
    };
}

export const CLOSE_CABINET = 'CLOSE_CABINET';
export function closeCabinet() {
    return {
        type: CLOSE_CABINET
    };
}

export const TOGGLE_INFOPOPUP = 'TOGGLE_INFOPOPUP';
export function toggleInfoPopup() {
    return {
        type: TOGGLE_INFOPOPUP
    };
}
