# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-20 14:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('archive', '0012_auto_20170320_0827'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personrole',
            name='role',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='archive.Role'),
        ),
    ]
