# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-20 14:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archive', '0011_auto_20170320_0823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personrole',
            name='comments',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='personrole',
            name='division',
            field=models.CharField(max_length=120, null=True),
        ),
        migrations.AlterField(
            model_name='personrole',
            name='full_name',
            field=models.CharField(max_length=120, null=True),
        ),
        migrations.AlterField(
            model_name='vehicle',
            name='name',
            field=models.CharField(max_length=60, unique=True),
        ),
    ]
