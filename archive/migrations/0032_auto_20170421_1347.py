# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-21 18:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archive', '0031_auto_20170421_1305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tag',
            name='name',
            field=models.CharField(max_length=60),
        ),
    ]
