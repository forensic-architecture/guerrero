# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-07 10:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archive', '0025_auto_20170407_0447'),
    ]

    operations = [
        migrations.RenameField(
            model_name='personrole',
            old_name='division',
            new_name='section',
        ),
    ]
