# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 17:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manual_id', models.IntegerField(null=True)),
                ('name_key', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('rain', models.NullBooleanField(default=None)),
                ('timestamp', models.DateTimeField(verbose_name='Event date and time')),
                ('timestamp_precise', models.NullBooleanField(default=False)),
                ('publicly_accessible', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60, unique=True)),
                ('latitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('longitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('extra_detail', models.CharField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Narrative',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manual_id', models.IntegerField(blank=True, null=True)),
                ('name', models.CharField(blank=True, max_length=60, null=True)),
                ('xls_col_label', models.CharField(max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60)),
                ('label', models.CharField(blank=True, max_length=60)),
                ('page', models.IntegerField(blank=True, null=True)),
                ('link', models.URLField(blank=True, null=True)),
                ('time_point', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='SourceType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='TagType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60)),
                ('xls_col_label', models.CharField(max_length=200, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='tag',
            name='tag_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='archive.TagType'),
        ),
        migrations.AddField(
            model_name='source',
            name='source_type',
            field=models.ForeignKey(default=1, null=True, on_delete=django.db.models.deletion.CASCADE, to='archive.SourceType'),
        ),
        migrations.AddField(
            model_name='event',
            name='location',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='archive.Location'),
        ),
        migrations.AddField(
            model_name='event',
            name='narrative_id_1',
            field=models.ManyToManyField(related_name='narrative_id_1', to='archive.Narrative'),
        ),
        migrations.AddField(
            model_name='event',
            name='narrative_id_2',
            field=models.ManyToManyField(related_name='narrative_id_2', to='archive.Narrative'),
        ),
        migrations.AddField(
            model_name='event',
            name='source',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='archive.Source'),
        ),
        migrations.AddField(
            model_name='event',
            name='tags',
            field=models.ManyToManyField(to='archive.Tag'),
        ),
        migrations.AlterUniqueTogether(
            name='tag',
            unique_together=set([('name', 'tag_type')]),
        ),
        migrations.AlterUniqueTogether(
            name='event',
            unique_together=set([('name_key', 'manual_id')]),
        ),
    ]
