# http://gspread.readthedocs.io/en/latest/index.html
# https://console.developers.google.com/
import os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import configparser

scope = ['https://spreadsheets.google.com/feeds']

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CONFIG = os.path.join(BASE_DIR, 'lib/credentials/config.ini')

config = configparser.ConfigParser()
config.read(CONFIG)

# Data spreadsheet
CREDENTIAL_FILE = os.path.join(BASE_DIR, 'lib/credentials/' + config['SPREADSHEET']['CREDENTIAL_FILE'])

# Location
LATITUDE_COLUMN_LABEL = 'latitude'
LONGITUDE_COLUMN_LABEL = 'longitude'
LOCATION_NAME_COLUMN_LABEL = 'geo_accuracy'

# Narratives
NARRATIVE_1 = 'narrative_ID_01'
NARRATIVE_2 = 'narrative_ID_02'

# Tag types
tag_types = {
    'TAG_1': "tag_1: *Actors Involved_security forces*",
    'TAG_2': "tag_2: *Actors Involved_students*",
    'TAG_3': "tag_3: *Actors Involved_criminal organisation*",
    'TAG_4': "tag_4: *Actors Involved_other*",
    'TAG_5': "tag_5: *Actors Involved_vehicles*",
    'TAG_6': "tag_6: *type of incident*",
    'TAG_7': "tag_7: *communication*",
    'TAG_8': "tag_8: *Weapons_involved*",
    'TAG_9': "tag_8: *transmisor*",
    'TAG_10': "tag_9: *receptor*"
}

# Source labels
SOURCE = "source_name"

class WorksheetReader:
    """Basic Spreadsheet reader

    It connects to a spreadsheet given a key,
    and allows going through its records and make operations
    """

    def get_events(self, do_print=False):
        """ """

        event_list = []
        for record in self.records[1:]:
            tags1 = [tag.strip() for tag in record[tag_types['TAG_1']].split(',')]
            tags2 = [tag.strip() for tag in record[tag_types['TAG_2']].split(',')]
            tags3 = [tag.strip() for tag in record[tag_types['TAG_3']].split(',')]
            tags4 = [tag.strip() for tag in record[tag_types['TAG_4']].split(',')]
            tags5 = [tag.strip() for tag in record[tag_types['TAG_5']].split(',')]
            tags6 = [tag.strip() for tag in record[tag_types['TAG_6']].split(',')]
            tags7 = [tag.strip() for tag in record[tag_types['TAG_7']].split(',')]
            tags8 = [tag.strip() for tag in record[tag_types['TAG_8']].split(',')]

            timestamp_precise = None
            if record['boolean_1: *precise time value*'] == 'N':
                timestamp_precise = False
            elif record['boolean_1: *precise time value*'] == 'Y':
                timestamp_precise = True
            event = {
                'event_id': record['event_ID'],
                'name': record['event_summary'],
                'narrative1': record[NARRATIVE_1],
                'narrative2': record[NARRATIVE_2],
                'location': (record['geo_accuracy'], record['latitude'], record['longitude']),
                'date_time': record['date_time'],
                'timestamp_precise': timestamp_precise,
                'timecode_precision': record['timecode_precision'],
                'description': record['event_summary'],
                'source': record['source_name'],
                'tags': [tags1, tags2, tags3, tags4, tags5, tags6, tags7, tags8]
            }

            event_list.append(event)

        return event_list

    def get_coevents(self, do_print=False):
        """ Returns list of coevents """

        coevent_list = []
        for record in self.records[1:]:
            coevent = {
                'event_id': record['event_ID'],
                'location': { 'name': record['geo_accuracy'], 'latitude': record['latitude'], 'longitude': record['longitude'] },
                'transmitter': record['tag_8: *transmisor*'],
                'receiver':	record['tag_9: *receptor*']
            }

            coevent_list.append(coevent)

        return coevent_list

    def get_tab(self):
        """Takes a tab number and returns a worksheet tab"""
        return self.worksheet.worksheets()[tab_num]

    def get_all_records(self, firstRow=0, lastRow=1000000):
        """Get all records from the current worksheet"""
        records = self.worksheet.get_all_records()
        return records[firstRow:lastRow]

    def __init__(self, gc, key, tab_num):
        self.key = key
        self.worksheet = gc.open_by_key(self.key).worksheets()[tab_num]
        self.records = self.worksheet.get_all_records()

def main(spreadsheet_key, tab_num=0):
    SPREADSHEET_KEY = config['SPREADSHEET'][spreadsheet_key]
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIAL_FILE, scope)
    gc = gspread.authorize(credentials)

    worksheet = WorksheetReader(gc, SPREADSHEET_KEY, tab_num)

    return worksheet

if __name__ == '__main__':
    main()
