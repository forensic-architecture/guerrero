from django.core.management.base import BaseCommand
from archive.models import Narrative, Location, District, Tag, Event, CoEvent

from django.utils.encoding import force_text
from decimal import Decimal
from django.utils.dateparse import parse_datetime
from archive.management.lib import worksheet

#worksheet_key = 'COEVENT_SPREADSHEET_KEY'
giei1 = 'GIEI_I_COEVENTS'
giei2 = 'GIEI_II_COEVENTS'
gibler = 'GIBLER_COEVENTS'

class Command(BaseCommand):
    """
    Downloads event data from G spreadsheets and creates django records of
    those events that are connected by the same manual_id
    """
    help = 'Downloads hierarchy data from google spreadsheets and updates tables'

    def update_coevent_records(self, worksheet):
        """
        Creates a record of a event, together with each associated location, etc
        """
        records = worksheet.get_coevents()

        communications = {}

        for index, record in enumerate(records):
            print('updating record ', str(index), '...')
            eID = record['event_id']

            if isinstance(eID, int):
                if eID in communications:
                    communications[eID].append(record)
                else:

                    communications[eID] = [record]
            else:
                print('Event does not contain an event ID. Ignoring.')

        # Take only communications
        for event_id, events in communications.items():
            print(event_id, len(events))
            if len(communications[event_id]) > 0:
                event = communications[event_id][0]
                manual_id = event['event_id']

                event = Event.objects.filter(manual_id = manual_id).first()
                if event is None:
                    print('%s has not been found in events.' % str(manual_id))
                else:
                    if len(events[0]['transmitter']) > 0:
                        transmitter = events[0]['transmitter']
                        location_transmitter = self.get_location(events[0]['location'])
                    elif len(events) > 1:
                        if len(events[1]['transmitter']) > 0:
                            transmitter = events[1]['transmitter']
                            location_transmitter = self.get_location(events[1]['location'])
                        else:
                            transmitter = None
                            location_transmitter = None
                    else:
                        transmitter = None
                        location_transmitter = None

                    if len(events[0]['receiver']) > 0:
                        receiver = events[0]['receiver']
                        location_receiver = self.get_location(events[0]['location'])
                    elif len(events) > 1:
                        if len(events[1]['receiver']) > 0:
                            receiver = events[1]['receiver']
                            location_receiver = self.get_location(events[1]['location'])
                        else:
                            receiver = None
                            location_receiver = None
                    else:
                        receiver = None
                        location_receiver = None

                    transmitter_tag = Tag.objects.filter(name = transmitter).first()
                    receiver_tag = Tag.objects.filter(name = receiver).first()

                    coevent, created = CoEvent.objects.get_or_create(
                        manual_id = manual_id,
                        defaults = {
                            'manual_id': manual_id,
                            'event': event,
                            'location_receiver': location_receiver,
                            'location_transmitter': location_transmitter,
                            'transmitter': transmitter_tag,
                            'receiver': receiver_tag
                        }
                    )

        print("Done.")

    def getDistrict(self, location_name):
        district_name = ''
        if location_name[0][0:45] == 'escena del crimen-Juan Alvarez y Perif. Norte':
            district_name = 'escena del crimen-Juan Alvarez y Perif. Norte'
            district, created = District.objects.update_or_create(
                name = district_name,
                defaults = { 'name': district_name }
            )
            return district
        elif location_name[0][0:30] == 'escena del crimen-Santa Teresa':
            district_name = 'escena del crimen-Santa Teresa'
            district, created = District.objects.update_or_create(
                name = district_name,
                defaults = { 'name': district_name }
            )
            return district
        elif location_name[0][0:37] == 'escena del crimen-Palacio de Justicia':
            district_name = 'escena del crimen-Palacio de Justicia'
            district, created = District.objects.update_or_create(
                name = district_name,
                defaults = { 'name': district_name }
            )
            return district
        else:
            return None

    def get_location(self, location):
        """ Create or get location for one event record, coords """

        location_name = location['name'],
        district = self.getDistrict(location_name)
        SIXPLACES = Decimal(10) ** -6
        if not type(location['latitude']) is str:
            location, created = Location.objects.get_or_create(
                name = location_name[0],
                latitude = Decimal(location['latitude']).quantize(SIXPLACES),
                longitude = Decimal(location['longitude']).quantize(SIXPLACES),
                district = district,
                defaults = {
                    'name': location_name[0],
                    'latitude': Decimal(location['latitude']).quantize(SIXPLACES),
                    'longitude': Decimal(location['longitude']).quantize(SIXPLACES),
                    'district': district,
                }
            )
            return location
        else:
            return None

    def update_coevents(self):
        # First delete what is there
        self.delete_all_coevents()
        # Tab number is second argument, look up the spreadsheet
        print("Loading GIEI I datasheet...")
        worksheet_client = worksheet.main(giei1, 0)
        print("Done.")
        print("Creating coevent records...")
        self.update_coevent_records(worksheet_client)

        print("Loading GIEI II datasheet...")
        worksheet_client = worksheet.main(giei2, 0)
        print("Done.")
        print("Creating coevent records...")
        self.update_coevent_records(worksheet_client)

        print("Loading Gibler datasheet...")
        worksheet_client = worksheet.main(gibler, 0)
        print("Done.")
        print("Creating coevent records...")
        self.update_coevent_records(worksheet_client)

        print("Update to coevents completed.")

    def delete_all_coevents(self):
        print("Clearing current database...")
        CoEvent.objects.all().delete()
        print("Done.")

    def handle(self, *args, **options):
        reply = input('This will clear the current table of coevents and replace it with information in the spreadsheet.\n Are you sure you want to continue? [y/N]: ')
        if not reply == 'y':
            print('Ok.')
        else:
            self.update_coevents()
