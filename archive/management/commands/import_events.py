from django.core.management.base import BaseCommand
from archive.models import (Source, Narrative, Location, District,
    Tag, Event)
from django.utils.encoding import force_text
from django.db.utils import OperationalError
from decimal import Decimal
from django.utils.dateparse import parse_datetime
from archive.management.lib import worksheet

worksheet_key = 'SPREADSHEET_KEY'
giei1 = 'GIEI_I'
giei2 = 'GIEI_II'
gibler = 'GIBLER'
narratives_spreadsheet_key = 'NARRATIVE_SPREADSHEET_KEY'
tag_spreadsheet_key = 'TAG_DIRECTORY_KEY'
location_key = 'LOCATION_SPREADSHEET_KEY'

narratives_dict = {}
tags_dict = {}
location_dict = {}

class Command(BaseCommand):
    """
    Downloads event data from G spreadsheets and creates django records
    """
    help = 'Downloads hierarchy data from google spreadsheets and updates tables'

    def update_event_records(self, worksheet):
        """
        Creates record of all events, together with each associated location, tag, etc
        """
        records = worksheet.get_events()

        for index, record in enumerate(records):
            # Source
            source, created = Source.objects.update_or_create(
                name = record['source'],
                defaults = {
                    'name': record['source'],
                }
            )

            # Tags
            all_tags_in_event = self.get_tags_for_one_event(record['tags'])

            # Narratives
            narrative1 = self.get_narrative1_for_one_event(record)

            # Narrative 2 needs to match with a Tag, throw an error if not
            narrative2 = self.get_narrative2_for_one_event(record)

            # Location
            location = self.get_location_for_one_event(record)

            # Events
            #
            # Some events have no, or 'x' as timestamp
            timestamp = record['date_time']
            if timestamp in ('', 'x'):
                timestamp = None
            else:
                timestamp = parse_datetime(timestamp)

            # Log some info on screen
            if location is not None:
                print('Event', record['event_id'], ':', timestamp or 'undefined', ' at', location.name)
            else:
                print('Event', record['event_id'], ':', timestamp or 'undefined', ' at', location)

            # And finally, create the record
            if isinstance(record['event_id'], int):
                try:
                    event, created = Event.objects.update_or_create(
                        manual_id = record['event_id'],
                        name_key = record['name'][0:200],
                        defaults = {
                            'manual_id': record['event_id'],
                            'source': source,
                            'location': location,
                            'narrative_1': narrative1,
                            'narrative_2': narrative2,
                            'timestamp': timestamp,
                            'timestamp_precise': record['timestamp_precise'],
                            'timecode_precision': record['timecode_precision'],
                            'name': record['name'],
                            'name_key': record['name'][0:200],
                            'description': record['description'],
                        }
                    )
                    for tag in all_tags_in_event:
                        event.tags.add(tag['id'])
                except OperationalError:
                    print('EXCEPTION: ', record['event_id'])
            else:
                print('Event does not contain an event ID. Ignoring')

        print("Done.")

    def getDistrict(self, location_name):
        district_name = ''

        if location_name[0] in location_dict:
            district = location_dict[location_name[0]]['district']

            if district == 'Escena Juan Alvarez y Periferico Norte':
                district_name = 'Escena Juan Alvarez y Periferico Norte'
                district, created = District.objects.update_or_create(
                    name = district_name,
                    defaults = { 'name': district_name }
                )
                return district
            elif district == 'Escena Santa Teresa':
                district_name = 'Escena Santa Teresa'
                district, created = District.objects.update_or_create(
                    name = district_name,
                    defaults = { 'name': district_name }
                )
                return district
            elif district == 'Escena Palacio de Justicia':
                district_name = 'Escena Palacio de Justicia'
                district, created = District.objects.update_or_create(
                    name = district_name,
                    defaults = { 'name': district_name }
                )
                return district
            else:
                return None
        else:
            return None

    def create_narratives_dictionary(self, narrative_worksheet):
        """ Take all narratives records and create a dict for convenience"""

        narratives = narrative_worksheet.get_all_records()
        for narrative in narratives:
            narratives_dict[str(narrative['narrative_ID_02'])] = narrative['Source name']
            narratives_dict[str(narrative['narrative_ID_01'])] = narrative['Source name']
            print('Creating narrative %s' % narrative['Source name'])
        return narratives_dict

    def create_location_dictionary(self, location_worksheet):
        locations = location_worksheet.get_all_records()
        for location in locations:
            location_dict[location['geo_accuracy']] = { 'district': location['Area'] }
        return location_dict

    def create_tag_dictionary(self, tag_worksheet):
        """ Creates tag records for all tags in tag master spreadsheet.
            It also creates a dict for convenience."""

        tags = tag_worksheet.get_all_records()
        for tag in tags:
            if 'group' in tag:
                group = tag['group']
            else:
                group = ''
            if 'subgroup' in tag:
                subgroup = tag['subgroup']
            else:
                subgroup = ''

            # Create a django record for all tags in tag master sheet
            if len(tag['name'].strip()) > 1:
                tagObj, created = Tag.objects.get_or_create(
                    name = tag['name'].strip(),
                    public_name = tag['public_name'].strip(),
                    type = tag['type'],
                    group = group,
                    subgroup = subgroup,
                    defaults = {
                        'name': tag['name'].strip(),
                        'public_name': tag['public_name'].strip(),
                        'type': tag['type'],
                        'group': group,
                        'subgroup': subgroup,
                        'mentions': 0
                    }
                )

                # Put them all in a dictionary for our use within this file
                print('Creating tag %s' % tag['name'])
                tags_dict[tag['name'].strip()] = {
                    'name': tag['name'].strip(),
                    'public_name': tag['public_name'].strip(),
                    'type': tag['type'],
                    'group': group,
                    'subgroup': subgroup,
                    'tag': tagObj,
                    'id': tagObj.id
                }
        return tags_dict

    def get_tags_for_one_event(self, tag_cols):
        """ Gets all the tags from an event's fields"""

        all_tags = []
        for tag_col in tag_cols:
            for tag_name in tag_col:
                group = 'Other'
                subgroup = 'Other'
                if tag_name in tags_dict:
                    tag = tags_dict[tag_name.strip()]
                    all_tags.append(tag)
                    self.increase_tag_mentions(tag)
        return all_tags

    def increase_tag_mentions(self, tag):
        if len(tag['name'].strip()) > 1:
            tagObj, created = Tag.objects.get_or_create(
                name = tag['name'].strip(),
                type = tag['type'],
                group = tag['group'],
                subgroup = tag['subgroup'],
                defaults = {
                    'name': tag['name'].strip(),
                    'type': tag['type'],
                    'group': tag['group'],
                    'subgroup': tag['subgroup'],
                    'mentions': 0
                }
            )
            if not created:
                tagObj.mentions += 1
                tagObj.save()

    def get_narrative1_for_one_event(self, record):
        """ Gets an event's narrative1 and creates a Narrative object"""

        narrative1 = record['narrative1']
        if narrative1 in ('', 'x'):
            narrative1 = None

        narrative1, created = Narrative.objects.update_or_create(
            manual_id = narrative1,
            xls_col_label = 'narrative_ID_01',
            defaults = {
                'name': record['narrative1'],
                'xls_col_label': 'narrative_ID_01'
            }
        )
        return narrative1

    def get_narrative2_for_one_event(self, record):
        """ Creates a Tag object that will point to narrative 2 """
        narrative2_key = str(record['narrative2'])
        if narrative2_key in ('', 'x'):
            narrative2_key = None

        tag = Tag.objects.get(name=narratives_dict[narrative2_key])

        narrative2, created = Narrative.objects.update_or_create(
            manual_id = narrative2_key,
            xls_col_label = 'narrative_ID_02',
            tag = tag,
            defaults = {
                'name': record['narrative2'],
                'xls_col_label': 'narrative_ID_02',
                'tag': tag
            }
        )
        return Tag.objects.get(name=narratives_dict[narrative2_key])

    def get_location_for_one_event(self, record):
        """ Create or get location for one event record, coords """

        if not type(record['location'][1]) is str:
            location_name = record['location'][0],
            district = self.getDistrict(location_name)
            SIXPLACES = Decimal(10) ** -6
            location, created = Location.objects.get_or_create(
                name = location_name[0],
                latitude = Decimal(record['location'][1]).quantize(SIXPLACES),
                longitude = Decimal(record['location'][2]).quantize(SIXPLACES),
                district = district,
                defaults = {
                    'name': location_name[0],
                    'latitude': Decimal(record['location'][1]).quantize(SIXPLACES),
                    'longitude': Decimal(record['location'][2]).quantize(SIXPLACES),
                    'district': district,
                }
            )
        else:
            location = None
        return location

    def populate_fresh_database(self):
        """ Repopulate the tables in the database when it's fresh and clean """
        # Prepare tags and narratives first
        print("Loading tags and narratives.")
        narrative_worksheet_client = worksheet.main(narratives_spreadsheet_key)
        narratives_dict = self.create_narratives_dictionary(narrative_worksheet_client)
        tag_worksheet_client = worksheet.main(tag_spreadsheet_key)
        tags_dict = self.create_tag_dictionary(tag_worksheet_client)
        location_worksheet_client = worksheet.main(location_key)
        location_dict = self.create_location_dictionary(location_worksheet_client)

        print("Loading GIEI I data...")
        worksheet_client = worksheet.main(giei1, 0)
        print("Creating event records...")
        self.update_event_records(worksheet_client)
        print("Done.")

        print("Loading GIEI II data...")
        worksheet_client = worksheet.main(giei2, 0)
        print("Creating event records...")
        self.update_event_records(worksheet_client)
        print("Done.")

        print("Loading 'Una Historia Oral de la Infamia' data...")
        worksheet_client = worksheet.main(gibler, 0)
        print("Creating event records...")
        self.update_event_records(worksheet_client)
        print("Done.")

        print("Update to events completed.")

    def clear_current_database(self):
        """ Truncate tables - spreadsheets are single source of truth """

        print("Clearing current database...")
        Event.objects.all().delete()
        Narrative.objects.all().delete()
        Location.objects.all().delete()
        Source.objects.all().delete()
        Tag.objects.all().delete()
        print("Done.")

    def handle(self, *args, **options):
        reply = input('This will clear the current database of events and replace it with information in the spreadsheet.\n Are you sure you want to continue? [y/N]: ')
        if not reply == 'y':
            print('Ok.')
        else:
            self.clear_current_database()
            self.populate_fresh_database()
