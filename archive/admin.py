from django.contrib import admin

from .models import (Source, Tag, Narrative,
    Location, Event, HierarchyRole)

# Register your models here.
admin.site.register(Source)
admin.site.register(HierarchyRole)

class NarrativeAdmin(admin.ModelAdmin):
    list_display = ('manual_id', 'name', 'xls_col_label')

admin.site.register(Narrative, NarrativeAdmin)

class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'latitude', 'longitude')

admin.site.register(Location, LocationAdmin)

class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'type')

admin.site.register(Tag, TagAdmin)

class EventAdmin(admin.ModelAdmin):
    list_display = ('name_key', 'timestamp', 'source', 'publicly_accessible')
    list_filter = ['timestamp']
    search_fields = ['name_key', 'description']

admin.site.register(Event, EventAdmin)
