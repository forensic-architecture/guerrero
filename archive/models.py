from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

# ==============================================================================
#
# Event-related models
#
# ==============================================================================

class Source(models.Model):
    name = models.CharField(max_length=60, unique=True)
    label = models.CharField(max_length=60, blank=True)
    page = models.IntegerField(null=True, blank=True)
    link = models.URLField(max_length=200, blank=True, null=True)
    time_point = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.label

class Tag(models.Model):
    name = models.CharField(max_length=200)
    public_name = models.CharField(max_length=200)
    group = models.CharField(max_length=120, null=True)
    subgroup = models.CharField(max_length=120, null=True)
    type = models.CharField(max_length=60, null=False, default='Other')
    mentions = models.IntegerField(default=0)

    class Meta:
        unique_together = ('name', 'type', 'group', 'subgroup')

    def __str__(self):
        return self.name

class District(models.Model):
    name = models.CharField(max_length=120, unique=True)

    def __str__(self):
        return self.name

class Location(models.Model):
    name = models.CharField(max_length=120)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    extra_detail = models.CharField(max_length=200, blank=True, null=True)
    district = models.ForeignKey(District, null=True)

    class Meta:
        unique_together = ('name', 'district', 'latitude', 'longitude')

    def __str__(self):
        return self.name

class Narrative(models.Model):
    manual_id = models.IntegerField(null=True, blank=True)
    name = models.CharField(max_length=60, null=True, blank=True)
    xls_col_label = models.CharField(max_length=200, null=True)
    tag = models.OneToOneField(Tag, null=True)

    def __str__(self):
        if self.name is not None:
            return self.name
        else:
            return str(self.manual_id)

class Event(models.Model):
    manual_id = models.IntegerField(null=True)
    name_key = models.CharField(max_length=200)
    name = models.TextField()
    description = models.TextField()
    narrative_1 = models.ForeignKey(Narrative, related_name='narrative_1')
    narrative_2 = models.ForeignKey(Tag, related_name='narrative_2')
    location = models.ForeignKey(Location, null=True, related_name='events')
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
    rain = models.NullBooleanField(blank=True, null=True, default=None)
    timestamp = models.DateTimeField('Event date and time', blank=True, null=True)
    timestamp_precise = models.NullBooleanField(default=False, null=True)
    timecode_precision = models.TextField(default='E', blank=True)
    publicly_accessible = models.BooleanField(default=False)

    class Meta:
        unique_together = ('name_key', 'manual_id',)

    def __str__(self):
        return self.name

class CoEvent(models.Model):
    manual_id = models.IntegerField(null=False)
    event = models.ForeignKey(Event, null=False)
    location_transmitter = models.ForeignKey(Location, null=True, related_name='location_transmitter')
    location_receiver = models.ForeignKey(Location, null=True, related_name='location_receiver')
    transmitter = models.ForeignKey(Tag, null=True, related_name='transmitter')
    receiver = models.ForeignKey(Tag, null=True, related_name='receiver')
    publicly_accessible = models.BooleanField(default=False)

    def __str__(self):
        return str(self.manual_id)


# ==============================================================================
#
# Hierarchy-related models
#
# ==============================================================================

class Organization(models.Model):
    name = models.CharField(max_length=60)

    def __str__(self):
        return self.name

class Vehicle(models.Model):
    name = models.CharField(max_length=60, unique=True)

    def __str__(self):
        return self.name

# ==============================================================================
#
# MPTT Nested Set Hierarchy models
#
# ==============================================================================


class HierarchySection(models.Model):
    name = models.CharField(max_length=120)
    organization = models.ForeignKey(Organization)

    class Meta:
        unique_together = ('name', 'organization')

    def __str__(self):
        return self.name


class HierarchyRole(MPTTModel):
    name = models.CharField(max_length=120, null=False, blank=False)
    organization = models.ForeignKey(Organization)
    section = models.ForeignKey(HierarchySection, null=True, blank=True)
    identificator = models.CharField(max_length=120)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        unique_together = ('name', 'organization')

    def __str__(self):
        return self.name

class PersonRole(models.Model):
    manual_id = models.CharField(max_length=200, unique=True)
    full_name = models.CharField(max_length=120, null=True)
    role = models.ForeignKey(HierarchyRole, null=True, blank=True)
    role_certainty = models.CharField(max_length=1,blank=True)
    tag = models.ForeignKey(Tag, null=False)
    vehicle = models.ForeignKey(Vehicle, null=True, blank=True)
    organization = models.ForeignKey(Organization, null=True, blank=True)
    comments = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.manual_id
