from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from .models import (Location, CoEvent, Event, Tag, Source,
    Narrative, PersonRole, HierarchyRole)

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('id', 'name', 'latitude', 'longitude')

class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'type', 'group', 'subgroup')

class TagIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ['id']

class TagNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ['id', 'name']

class NarrativeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Narrative
        fields = ('id', 'manual_id')

class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ('id', 'name')

class EventSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    tags = TagIdSerializer(many=True)

    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data. """

        # select_related for "to-one" relationships
        queryset = queryset.select_related('location')

        # prefetch_related for "to-many" relationships
        queryset = queryset.prefetch_related('tags')

        return queryset

    class Meta:
        model = Event
        fields = ('id', 'manual_id', 'location', 'timestamp', 'tags',
            'narrative_2')

class EventDetailSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    tags = TagIdSerializer(many=True)
    source = SourceSerializer()
    narrative_2 = NarrativeSerializer()

    class Meta:
        model = Event
        fields = ('id', 'manual_id', 'name', 'location', 'timestamp', 'narrative_2',
            'timestamp_precise', 'description', 'tags', 'source')

class MiniEventSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    tags = TagIdSerializer(many=True)

    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data. """

        # select_related for "to-one" relationships
        queryset = queryset.select_related('location')

        # prefetch_related for "to-many" relationships
        queryset = queryset.prefetch_related('tags')

        return queryset

    class Meta:
        model = Event
        fields = ('id', 'manual_id', 'location', 'timestamp', 'tags')

class EventTimeSerializer(serializers.ModelSerializer):
    tags = TagIdSerializer(many=True)

    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data. """

        # prefetch_related for "to-many" relationships
        queryset = queryset.prefetch_related('tags')

        return queryset

    class Meta:
        model = Event
        fields = ('id', 'manual_id', 'timestamp', 'tags')

class EventLocSerializer(serializers.ModelSerializer):
    events = EventTimeSerializer(read_only=True, many=True)
    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data. """

        # prefetch_related for "to-many" relationships
        queryset = queryset.prefetch_related('events')
        return queryset

    class Meta:
        model = Location
        fields = ('id', 'name', 'latitude', 'longitude', 'events')

class CoEventSerializer(serializers.ModelSerializer):
    event_1 = MiniEventSerializer()
    event_2 = MiniEventSerializer()

    class Meta:
        model = CoEvent
        fields = ('id', 'manual_id', 'event_1', 'event_2')

class HierarchyRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = HierarchyRole
        fields = ('id', 'name', 'children', 'organization_id')
