# coding=utf-8

from collections import defaultdict

from django.shortcuts import render, HttpResponse, redirect
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.views import logout
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db.models import F

from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework import status, generics

import json
import requests

from .models import CoEvent, Event, Location, District, Tag, Narrative


# USER AUTHENTICATION AND LOGIN
# ------------------------------------------------------------------------------

def user_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        print(request)
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return redirect('/')
            else:
                return Response("Your account is disabled.")
        else:
            print("Invalid login details.")
            return render(request, 'archive/login.html',
                { 'message': 'Información de registro no válida. Por favor, vuelva a intentarlo.' })
    else:
        return render(request, 'archive/login.html', {})

def logout_view(request, *args, **kwargs):
    return logout(request, *args, **kwargs)


# MAIN INDEX SITE
# ------------------------------------------------------------------------------

def index(request):
    return render(request, 'archive/index.html')


# REST Framework Serializer
# DRF works well with class-based views
# Think of ViewSets as controllers...
# ------------------------------------------------------------------------------

class EventsList(generics.ListAPIView):
    """
    API Endpoint: Returns normalized tables of events, coevents,
    tags, locations, districts, sites, attacks and narratives
    """

    def get_tag_lists(self):
        """
        Returns a normalized table of tags, groups, subgroups and types
        """
        tag_list = Tag.objects.all().values('type', 'subgroup', 'group', 'id', 'mentions', 'public_name').annotate(name=F('public_name'))
        tags = { "byId": {}, "allIds": [] }
        tag_types = { "byId": {}, "allIds": [] }
        tag_groups = { "byId": {}, "allIds": [] }
        tag_subgroups = { "byId": {}, "allIds": [] }

        # Separate tables for each level of tag hierarchy
        for tag in tag_list:
            tags["byId"][tag["id"]] = tag
            tags["allIds"].append(tag["id"])

            if tag["type"] not in tag_types["byId"]:
                tag_types["byId"][tag["type"]] = { "id": tag["type"], "groups": [tag["group"]] }
                tag_types["allIds"].append(tag["type"])
            elif tag["group"] not in tag_types["byId"][tag["type"]]["groups"]:
                tag_types["byId"][tag["type"]]["groups"].append(tag["group"])

            if tag["group"] not in tag_groups["byId"]:
                tag_groups["byId"][tag["group"]] = { "id": tag["group"], "subgroups": [tag["subgroup"]] }
                tag_groups["allIds"].append(tag["group"])
            elif tag["subgroup"] not in tag_groups["byId"][tag["group"]]["subgroups"]:
                tag_groups["byId"][tag["group"]]["subgroups"].append(tag["subgroup"])

            if tag["subgroup"] not in tag_subgroups["byId"]:
                tag_subgroups["byId"][tag["subgroup"]] = { "id": tag["subgroup"], "tags": [tag["id"]] }
                tag_subgroups["allIds"].append(tag["subgroup"])
            elif tag["id"] not in tag_subgroups["byId"][tag["subgroup"]]["tags"]:
                tag_subgroups["byId"][tag["subgroup"]]["tags"].append(tag["id"])

        return {
            "tag_types": tag_types,
            "tag_groups": tag_groups,
            "tag_subgroups": tag_subgroups,
            "tags": tags
        }

    def get_events(self):
        """
        Returns a normalized table of events and their tags
        """
        event_list = Event.objects.all().prefetch_related('tags')
        events = { "byId": {}, "allIds": { "located": [], "unlocated": [] } }

        for event in event_list:
            tag_list = event.tags.all().values_list('id', flat=True)
            events["byId"][event.manual_id] = {
                "id": event.manual_id,
                "timestamp": event.timestamp,
                "location": event.location_id,
                "tags": list(tag_list),
                "narrative_2": event.narrative_2_id
            }
            if event.location_id is None:
                events["allIds"]["unlocated"].append(event.manual_id)
            else:
                events["allIds"]["located"].append(event.manual_id)
        return events

    def get_coevents(self):
        """
        Returns a normalized table of coevents
        """

        coevent_list = CoEvent.objects.all().prefetch_related('event')
        coevents = { "byId": {}, "allIds": [] }

        for coevent in coevent_list:
            manual_id = coevent.manual_id
            coevents["allIds"].append(manual_id)
            coevents["byId"][manual_id] = {
                "id": manual_id,
                "event": coevent.manual_id,
                "location_receiver": coevent.location_receiver_id,
                "location_transmitter": coevent.location_transmitter_id,
                "transmitter": coevent.transmitter_id,
                "receiver": coevent.receiver_id
            }

        return coevents

    def get_locations(self):
        """
        Returns a normalized table of locations
        """
        location_list = Location.objects.all().prefetch_related('events')
        locations = { "byId": {}, "allIds": [] }

        for location in location_list:
            event_set = location.events.all().values_list('manual_id', flat=True)
            locations["byId"][location.id] = {
                "id": location.id,
                "latitude": location.latitude,
                "longitude": location.longitude,
                "name": location.name,
                "events": list(event_set)
            }
            locations["allIds"].append(location.id)
        return locations

    def get_districts(self):
        """
        Returns a normalized table of districts and their locations
        """
        district_list = District.objects.all().prefetch_related('location_set')
        districts = { "byId": {}, "allIds": [] }

        for district in district_list:
            location_set = district.location_set.all().values_list('id', flat=True)
            districts["byId"][district.id] = {
                "id": district.id,
                "locations": list(location_set)
            }
            districts["allIds"].append(district.id)
        return districts

    def get_attacks(self):
        with open('archive/static/archive/data/attacks.json') as data_file:
            data = json.load(data_file)
            return data

    def get_sites(self):
        with open('archive/static/archive/data/sites.json') as data_file:
            data = json.load(data_file)
            return data

    def get_bus_routes(self):
        with open('archive/static/archive/data/routes.json') as data_file:
            data = json.load(data_file)
            return data

    def get_narratives(self, events, tags):
        #narrativeIds = self.get_curated_narratives()
        narrative_list = Narrative.objects.all().prefetch_related('tag')
        narratives = { "byId": {}, "allIds": [] }

        eventPoints = defaultdict(dict);

        for narrative in narrative_list:
            if narrative.xls_col_label == "narrative_ID_02":
                narratives["byId"][narrative.tag_id] = {
                    "name": narrative.tag.public_name,
                    "id": narrative.tag.id,
                    "counts": 0,
                    "events": []
                }
                narratives["allIds"].append(narrative.tag_id)

        for event_id in events["allIds"]["located"] + events["allIds"]["unlocated"]:
            event = events["byId"][event_id]
            narrative_id = tags["byId"][event["narrative_2"]]["id"]
            if narrative_id in narratives["byId"]:
                narratives["byId"][narrative_id]["events"].append(event_id)
                narratives["byId"][narrative_id]["counts"] += 1

        return narratives

    def get(self, request):
        tag_lists = self.get_tag_lists()
        events = self.get_events()
        coevents = self.get_coevents()
        locations = self.get_locations()
        districts = self.get_districts()
        sites = self.get_sites()
        routes = self.get_bus_routes()
        attacks = self.get_attacks()
        narratives = self.get_narratives(events, tag_lists["tags"])

        return JsonResponse({
            "events": events,
            "coevents": coevents,
            "tags": tag_lists["tags"],
            "tag_types": tag_lists["tag_types"],
            "tag_groups": tag_lists["tag_groups"],
            "tag_subgroups": tag_lists["tag_subgroups"],
            "locations": locations,
            "districts": districts,
            "narratives": narratives,
            "attacks": attacks,
            "sites": sites,
            "routes": routes
        })

class EventDetail(generics.GenericAPIView):
    """
    API Endpoint: Returns a detailed Event object
    """
    def get(self, request, event_id):
        event = Event.objects.filter(manual_id=event_id).prefetch_related('tags').prefetch_related('source').prefetch_related('location').first()
        tags = event.tags.all().values_list('id', flat=True)
        source = event.source.name
        if event.location is not None:
            location = { "id": event.location.id, "latitude": event.location.latitude, "longitude": event.location.longitude, "name": event.location.name }
        else:
            location = None

        return JsonResponse({
            "id": event_id,
            "name": event.name,
            "location": location,
            "timestamp": event.timestamp,
            "timecode_precision": event.timecode_precision,
            "description": event.description,
            "tags": list(tags),
            "narrative": event.narrative_2_id,
            "source": source,
        })

class SearchList(generics.GenericAPIView):

    def get(self, request, query):
        tags = Tag.objects.filter(public_name__icontains=query).values('type', 'subgroup', 'group', 'id', 'mentions', 'public_name').annotate(name=F('public_name'))

        return JsonResponse({
            "tags": list(tags)
        })

# MAPPING PROXY
# ------------------------------------------------------------------------------

class MappingProxy(generics.GenericAPIView):
    def get(self, request, z, x, y):

        #access_token = 'pk.eyJ1IjoiZmFkZXYiLCJhIjoiY2o2eXNodXpnMjJnbzJybXRmMWtwajVrdyJ9.djCM8zBqCtA-SC_EChxtzQ'
        access_token = 'pk.eyJ1IjoiZmFkZXYiLCJhIjoiY2pyMjNiZHdvMHVjcjQ5cnU0b2dpdGRxZCJ9.7bci8C-z3EVCa_yMZKymeg'
        urlpath = 'http://a.tiles.mapbox.com/v4/mapbox.satellite/'
        url = urlpath + z + '/' + x + '/' + y + '@2x.png?'
        url = url + 'access_token=' + access_token

        r = requests.get(url)

        return HttpResponse(r.content, content_type="image/png")
