window.addEventListener("load", main, false);

function main() {

    //var url = "http://104.236.165.84/static/app3d/data/scenes/ja/";
    var url = "http://127.0.0.1:8000/static/app3d/data/scenes/ja/";
    var scene = new Scene(url);

    window.addEventListener(
            'keydown',
            (event) => {
                var key = event.key;
                scene.reload(key);
            },
            false
    );

}
