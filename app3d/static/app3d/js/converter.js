class Convertor {

  constructor(global, local) {
    this.global = {};
    this.global.min = this.project(global.min);
    this.global.max = this.project(global.max);
    this.local = local;
  }

  convert(target) {
    return {
      x: this.lerp(
             this.global.min.x,
             this.global.max.x,
             this.project(target).x,
             this.local.min.x,
             this.local.max.x
             ),
      y: 0,
      z: this.lerp(
          this.global.min.z,
          this.global.max.z,
          this.project(target).z,
          this.local.min.z,
          this.local.max.z
          )
    };
  }

  lerp(a1, a2, a, b1, b2) {
    return b1 + (b2 - b1) * (a - a1) / (a2 - a1);
  }

  project(latlng) {
    var siny = Math.sin(latlng.lat * Math.PI / 180);
    siny = Math.min(Math.max(siny, -0.9999), 0.9999);

    return {
      x: 0.5 + latlng.lng / 360,
      y: 0.0,
      z: 0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)
    };
  }
}
