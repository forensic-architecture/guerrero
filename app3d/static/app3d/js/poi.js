class Poi {

    constructor( metadata, bbox ) {
        this.metadata = metadata; // locations with events.
        this.name = metadata.name;
        this.geometry = null;
        this.text = null;

        var convertor = new Convertor(bbox.global, bbox.local);
        var coord = convertor.convert( {lat: metadata.latitude, lng: metadata.longitude} );
        var position = new THREE.Vector3(coord.x, this.getRandomArbitrary( 1.0, 5.0 ), coord.z);

        this.colorTextDefault = new THREE.Color().setHSL( .16, 1.0, .57 );
        //this.colorGeomDefault = new THREE.Color().setHSL( .15, .6, .69 );
        this.colorGeomDefault = new THREE.Color().setHSL( .16, .19, .57 );

        var setText = (eventsNum) => {
            this.text = new THREE.TextSprite( {
                textSize: 0.8,
                redrawInterval: 1,
                roundFontSizeToNearestPowerOfTwo: true,
                material: {
                    color: this.colorTextDefault,
                    opacity: 1.0,
                    transparent: true,
                },
                texture:{
                    text: eventsNum,
                    fontFamily: 'Verdana, Geneva, sans-serif'
                }
            });
            this.text.position.x = position.x;
            this.text.position.y = position.y;
            this.text.position.z = position.z;
        }
        setText( (this.metadata.events.length).toString() );

        var setGeometry = ( position ) => {
            var material = new THREE.MeshPhongMaterial({
                color: this.colorGeomDefault, 
                transparent: true,
                opacity:.87
            });
            this.geometry = new THREE.Mesh( new THREE.CircleBufferGeometry( .2, 35 ), material );
            this.geometry.position.set( position.x, position.y, position.z );
            this.geometry.name = this.name; // potentially used for removing from THREE pois scene.
        }
        setGeometry( position );
    }

    isVisible() {
        if( this.geometry ) return this.geometry.visible;
        return false;
    }

    show() {
        if( this.geometry )
            this.geometry.visible = true;
        if( this.text )
            this.text.visible = true;
    }

    hide() {
        if( this.geometry )
            this.geometry.visible = false;
        if( this.text )
            this.text.visible = false;
    }

    getRandomArbitrary( min, max ) {
        return Math.random() * ( max - min ) + min;
    }

    updateOpacityAndScale( camera ) {
        var solidDistance = 20;

        var position = this.geometry.position.clone();
        var cameraPosition = camera.position.clone();

        var distance = position.distanceTo( cameraPosition );
        if( distance < solidDistance )
            distance = solidDistance;

        var opacity = Math.min( solidDistance / distance, .40 );
        var scale = Math.max( 3.0, distance*.097 );

        this.updateOpacity( opacity );
        this.updateScale( scale );
    }

    updateOpacity( opacity ) {
        if( this.text )
            this.text.material.opacity = Math.max( .55, opacity*1.5 );
        if( this.geometry )
            this.geometry.material.opacity = opacity;//this.text.material.opacity;
    }

    updateScale( scale ) {
        if( this.geometry )
            this.geometry.scale.set( scale, scale, scale );
        if( this.text )
            this.text.textSize = Math.max( scale*.25, 1.1 );
    }

    updateMetadata( metadata ) {
        var prevEventsNum = this.metadata.events.length;
        this.metadata = metadata;
        if( prevEventsNum !== this.metadata.events.length )
            this.text.material.map.text = (this.metadata.events.length).toString(); // Update the displayed events number.
    }

    getGeometry() {
        return this.geometry;
    }


    getText() {
        return this.text;
    }

    getPosition() {
        return this.geometry.position;
    }

    setColor( color ) {
        this.geometry.material.color = color;
        this.text.material.color = color;
    }

    setDefaultColor() {
        this.geometry.material.color = this.colorGeomDefault;
        this.text.material.color = this.colorTextDefault;
    }

    getName() {
        return this.name;
    }

    getMetadata() {
        return this.metadata;
    }
    setRenderOrder( renderOrder ) {
        this.geometry.renderOrder = renderOrder;
    }
};
