from django.shortcuts import render


def index(request):
    return render(request, "app3d/index.html")
