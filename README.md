# The Ayotzinapa Platform

## By Forensic Architecture, 2017

Development: Franc Camps Febrer, Petros Kataras, Emmanouil Matsis
Design: Camilo Nestor Vargas, Franc Camps Febrer

The Ayotzinapa Platform is an interactive tool and experience that allows users to expore and analyze data regarding the disappearance of 43 students in the town of Iguala, Guerrero (Mexico) on the night of the 26th to the 27th of September of 2014.

The platform is mostly composed of two parts: a 2D mapping tool to visualize the variety of data events over time, and a 3D scene exploration tool to focus and explore 3 crime scene in 3D, as well as data events associated with it.

## Development

### Database management

The Django application connects to a MySQL database. Django looks for connection configuration properties in a file called `settings.ini`, which you'll need to create. In the root folder of the project there is a file called `settings_SAMPLE.ini`. Copy this file and name it `settings.ini`, and complete it with the data of your local MySQL environment.

After you've created a blank database and set up the Django application to connect to it, you'll need to populate it with data. The data in the database comes from a series of spreadsheets in Google Spreadsheets. There are two ways to populate the database: one is to use a mysql dump backup, and the other is using a series of `manage.py` custom commands specifically written to download data from the spreadsheets and populate the db.

If you don't have access to a sql dump, you can use Django custom commands. These commands rely on the information in `settings.ini` to populate the database. Make sure to run them in the following order.

```shell
python manage.py import_events
python manage.py import_coevents
```

It may take a little bit, but then your database should be ready to go.

### Running the Django application locally

The application is using Django version 1.10.
Start a virtual environment in your local for the project. Once in it, install all dependencies. The application runs on python 3, so depending on your setup (if you have more than one Python installation installed) you may want to use `pip` or `pip3`.

```shell
pip3 install -r requirements.txt
```

Once all the dependencies are installed, you can run the local development environment by doing: 

```shell
python3 manage.py runserver 8000
```

And then access the application

### Building the React application

The React application is bundled and built using Webpack, via an NPM script. From the `frontend` folder, do:

```shell
npm run dev
```

To build a development bundle and watch for new changes. For the actual production built, do:

```shell
npm run build
```

#### Mapping dependencies

The 2D mapping tool relies on Mapbox for satellite imagery, and does so live. In order to properly connect to the Mapbox API, you'll need to add a file called `config.json` in the root folder of the following form, with your Mapbox API key:

```json
{
  "mapbox_accessToken": "<YOUR_MAPBOX_ACCESS_TOKEN>"
}
```

This should be good to go.
